<?php
class ControllerStartupStartup extends Controller {
	public function index() {
		
        // Server Link
        if ($this->request->server['HTTPS']) {
			$this->config->set('serverLink', HTTPS_SERVER);
            $this->config->set('config_ssl', HTTPS_SERVER);
		} else {
			$this->config->set('serverLink', HTTP_SERVER);
            $this->config->set('config_url', HTTP_SERVER);
		}
        
        // Url
		$this->registry->set('url', new Url($this->config->get('serverLink'), $this->config->get('serverLink')));
        
	}
}
