<?php
// header
$_['heading_title']  = '樂咖管理平台';

// Text
$_['text_heading']   = '樂咖管理平台';
$_['text_login']     = '樂咖管理平台';
$_['text_forgotten'] = '忘記密碼';

// Entry
$_['entry_username'] = '使用者帳號';
$_['entry_password'] = '使用者密碼';

// Button
$_['button_login']   = '登入';

// Error
$_['error_login']    = '使用者帳號或密碼錯誤';
$_['error_token']    = 'Token Session無效,請重新登入';