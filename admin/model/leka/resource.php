<?php
class ModelLekaResource extends Model {

	/**
	 * [getCommissionRate description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-08
	 */
	public function getCommissionRate () {
		return array(
				"main_rate"      => '通路商主要比例',
				"kmfun_rate"     => '司機代駕比例',
				"transfer_rate"  => '金豐接送比例',
				"other_rate"     => '其他比例',
			) ;
	}

	/**
	 * [getList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-25
	 */
	public function getResourceList( $data = array()) {
		$whereStr = "" ;
		if ( isset( $data['source_name'])) {
			if (!empty($data['source_name'])) {
				$whereStr .= "source_name LIKE '%" . $this->db->escape($data['source_name']) . "%'";
			}
		}

		if ( !empty( $whereStr)) $whereStr = "WHERE {$whereStr}" ;
		$SQLCmd = "SELECT idx, source_name, parent, main_rate, deduct_rate, kmfun_rate, transfer_rate,
						agency_rate, other_rate, agency_name, account_name FROM tb_resource {$whereStr}" ;

		$sort_data = array(
			"source_name",
			"main_rate",
			"deduct_rate",
			"kmfun_rate",
			"transfer_rate",
			"agency_rate",
			"other_rate",
		);
        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $SQLCmd .= " ORDER BY " . $data['sort'];
			if (( $data['order'] == 'DESC')) {
				$SQLCmd .= " DESC";
			} else {
				$SQLCmd .= " ASC";
			}
        } else {
            $SQLCmd .= "" ;
        }
		return $this->db->query( $SQLCmd)->rows ;
	}

	/**
	 * [getInfoForIdx description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-02-10
	 */
	public function getInfoForIdx( $data = array()) {
		$SQLCmd = "SELECT idx, source_name, account, email, receive, account_name, bank_name, bank_branch,
					bank_code, bank_account FROM tb_resource WHERE idx=" . (int)$this->db->escape($data['idx']) ;
		return $this->db->query( $SQLCmd)->row ;
	}

	/**
	 * [checkAccountExists description]
	 * @param   string     $account [description]
	 * @return  [type]              [description]
	 * @Another Angus
	 * @date    2019-02-10
	 */
	public function checkAccountExists( $source_id = "", $account = "") {
		if ( !empty($source_id)) {
			$andStr = "AND idx !=". $this->db->escape( $source_id) ;
		} else {
			$andStr = "" ;
		}

		$SQLCmd = "SELECT * FROM tb_resource WHERE account='" . $this->db->escape( $account). "'" . $andStr ;
		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd)->num_rows ;
	}

	/**
	 * [addResource description]
	 * @param   array      $data [description]
	 * @Another Angus
	 * @date    2019-02-13
	 */
	public function addResource( $data = array()) {
		$setStr = "" ;

		$setStr .= !empty(trim( $data['nDetails_source_name'])) ? "source_name='".$this->db->escape(trim($data['nDetails_source_name']))."'," : "" ;
		$setStr .= !empty(trim( $data['nDetails_account'])) ? "account='".$this->db->escape(trim($data['nDetails_account']))."'," : "" ;
		$setStr .= !empty(trim( $data['nDetails_password'])) ? "password='".$this->db->escape(trim($data['nDetails_password']))."'," : "" ;
		$setStr .= !empty(trim( $data['nDetails_email'])) ? "email='".$this->db->escape(trim($data['nDetails_email']))."'," : "" ;

		// $setStr .= !empty(trim( $data['rate_main'])) ? "rate_main='".$this->db->escape(trim($data['rate_main']))."'," : "" ;
		// $setStr .= !empty(trim( $data['rate_deduct'])) ? "rate_deduct='".$this->db->escape(trim($data['rate_deduct']))."'," : "" ;
		// $setStr .= !empty(trim( $data['rate_kmfun'])) ? "rate_kmfun='".$this->db->escape(trim($data['rate_kmfun']))."'," : "" ;
		// $setStr .= !empty(trim( $data['rate_transfer'])) ? "rate_transfer='".$this->db->escape(trim($data['rate_transfer']))."'," : "" ;
		// $setStr .= !empty(trim( $data['rate_agency'])) ? "rate_agency='".$this->db->escape(trim($data['rate_agency']))."'," : "" ;

		$setStr .= !empty(trim( $data['nDetails_receive'])) ? "receive='".$this->db->escape(trim($data['nDetails_receive']))."'," : "" ;
		$setStr .= !empty(trim( $data['nDetails_account_name'])) ? "account_name='".$this->db->escape(trim($data['rDetails_account_name']))."'," : "" ;
		$setStr .= !empty(trim( $data['nDetails_bank_name'])) ? "bank_name='".$this->db->escape(trim($data['nDetails_bank_name']))."'," : "" ;
		$setStr .= !empty(trim( $data['nDetails_bank_branch'])) ? "bank_branch='".$this->db->escape(trim($data['rDetails_bank_branch']))."'," : "" ;
		$setStr .= !empty(trim( $data['nDetails_bank_code'])) ? "bank_code='".$this->db->escape(trim($data['nDetails_bank_code']))."'," : "" ;
		$setStr .= !empty(trim( $data['nDetails_bank_account'])) ? "bank_account='".$this->db->escape(trim($data['nDetails_bank_account']))."'," : "" ;

		$setStr .= "reg_user='".$this->user->getUserName()."'," ;
		$setStr .= "reg_date=now()," ;

		$setStr = substr( $setStr, 0, strlen( $setStr)-1) ;

		$SQLCmd = "INSERT INTO tb_resource SET {$setStr}" ;
		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd) ;
	}

	/**
	 * [editResource description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-02-10
	 */
	public function editResource( $data = array()) {
		$setStr = "" ;

		if ($data['rDetails_password']) {
			$setStr .= "password='".$this->db->escape(trim($data['rDetails_password']))."'," ;
		}

		$setStr .= "receive='".$this->db->escape(trim($data['rDetails_receive']))."'," ;
		$setStr .= "account_name='".$this->db->escape(trim($data['rDetails_account_name']))."'," ;
		$setStr .= "bank_name='".$this->db->escape(trim($data['rDetails_bank_name']))."'," ;
		$setStr .= "bank_branch='".$this->db->escape(trim($data['rDetails_bank_branch']))."'," ;
		$setStr .= "bank_code='".$this->db->escape(trim($data['rDetails_bank_code']))."'," ;
		$setStr .= "bank_account='".$this->db->escape(trim($data['rDetails_bank_account']))."'," ;
		$setStr .= "re_user='".$this->user->getUserName()."'," ;
		$setStr .= "re_date=now()," ;

		$setStr = substr( $setStr, 0, strlen( $setStr)-1) ;

		$SQLCmd = "UPDATE tb_resource SET {$setStr} WHERE idx=". $this->db->escape($data['rDetails_idx']) ;
		// dump($this->db->query( $SQLCmd));
		return $this->db->query( $SQLCmd) ;
	}

	/**
	 * [editRowSource description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-02-11
	 */
	public function editRowSource( $data = array()) {
		foreach ($data as $idx => $row) {
			$setStr = "" ;
			$setStr .= "source_name='".$this->db->escape(trim($row['source_name']))."'," ;

			if ( isset( $row['main_rate'])) {
				$setStr .= "main_rate='".$this->db->escape(floatval( $row['main_rate']))."'," ;
			}

			// 2019.11.05
			// if ( !empty( floatval(trim($row['deduct_rate'])))) {
			// 	$setStr .= "deduct_rate='".$this->db->escape(floatval(trim($row['deduct_rate'])))."'," ;
			// }

			if ( isset( $row['kmfun_rate']) ) {
				$setStr .= "kmfun_rate='".$this->db->escape(floatval( $row['kmfun_rate']))."'," ;
			}

			if ( isset( $row['transfer_rate'])) {
				$setStr .= "transfer_rate='".$this->db->escape(floatval( $row['transfer_rate']))."'," ;
			}

			// 2019.11.05
			// if ( !empty( floatval(trim($row['agency_rate'])))) {
			// 	$setStr .= "agency_rate='".$this->db->escape(floatval(trim($row['agency_rate'])))."'," ;
			// }

			if ( isset( $row['other_rate'])) {
				$setStr .= "other_rate='".$this->db->escape(floatval( $row['other_rate']))."'," ;
			}
			$setStr .= "re_user='".$this->user->getUserName()."'," ;
			$setStr .= "re_date=now()," ;

			$setStr = substr( $setStr, 0, strlen( $setStr)-1) ;
			$SQLCmd = "UPDATE tb_resource SET {$setStr} WHERE idx=". $this->db->escape( $idx) ;
			// dump( $SQLCmd) ;
			$this->db->query( $SQLCmd) ;
		}
	}

	/**
	 * [editAccount description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-02-10
	 */
	public function editAccount( $data = array()) {
		$SQLCmd = "UPDATE tb_resource SET account='". $this->db->escape($data['rDetails_account']) ."' WHERE idx=". $this->db->escape($data['rDetails_idx']) ;
		return $this->db->query( $SQLCmd) ;
	}


}