<?php
class ModelLekaSummary extends Model {
    /**
     * return stdClass Object
     * [num_rows] => 3
     * [row] => Array
     * [rows] => Array(
     *        [0] => Array
     *        [1] => Array
     * )
     */
	/**
	 * [getList 退傭日誌List]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-25
	 */
	public function getSummaryList( $data = array()) {
		// dump( $data) ;
		$sort_data = array() ;
		// $whereStr = " DATE_FORMAT(CONCAT(syear,'-',smonth,'-',sday), '%Y-%m-%d') = '{$data['input_search_date']}'" ;
		$column = array();
		if ( isset($data['filter_syear']) && $data['filter_syear'] != "") {
			$column[] = "syear='".$this->db->escape($data['filter_syear'])."'";
		}
		if ( isset($data['filter_smonth']) && $data['filter_smonth'] != "") {
			$column[] = "smonth='".$this->db->escape($data['filter_smonth'])."'";
		}
		if ( isset($data['filter_sday']) && $data['filter_sday'] != "") {
			$column[] = "sday='".$this->db->escape($data['filter_sday'])."'";
		}
		if ( isset($data['filter_platform']) && $data['filter_platform'] != "") {
			$column[] = "platform='".$this->db->escape($data['filter_platform'])."'";
		}
		if ( isset($data['filter_guest_source']) && $data['filter_guest_source'] != "") {
			$column[] = "guest_source like '%".$this->db->escape($data['filter_guest_source'])."%'";
		}

		$SQLCmd = "SELECT * FROM tb_summary" ;
		if(count($column)) {
			$SQLCmd .= " WHERE " . implode(" AND ",$column);
		}

		$sort_data = array(
			"cardno",
			"makecard",
			"guest_source",
			"guest_name",
			"people",
			"consume",
			"commission",
		);
        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $SQLCmd .= " ORDER BY " . $data['sort'];
			if (( $data['order'] == 'DESC')) {
				$SQLCmd .= " DESC";
			} else {
				$SQLCmd .= " ASC";
			}
        } else {
            $SQLCmd .= " ORDER BY platform ASC, syear ASC, smonth ASC, sday ASC" ;
        }

		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd)->rows ;
	}

	/**
	 * [getRefundGroup 退傭總表]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-01-30
	 */
	public function getRefundLists( $data = array()) {
		// dump( $data) ;
		$tbsArr = array() ;
		$tbrArr = array() ;
		$tb_summaryWhere = "" ;
		$tb_resourceWhere = "" ;

		if ( isset($data['filter_platform']) && !empty( $data['filter_platform']) ) {
			$tbsArr[] = " platform=". $this->db->escape( $data['filter_platform']) ;
		}
		if ( isset($data['filter_syear']) && !empty( $data['filter_syear']) ) {
			$tbsArr[] = " syear=". $this->db->escape( $data['filter_syear']) ;
		}
		if ( isset($data['filter_smonth']) && !empty( $data['filter_smonth']) ) {
			$tbsArr[] = " smonth=". $this->db->escape( $data['filter_smonth']) ;
		}
		if ( isset($data['filter_name']) && !empty( $data['filter_name']) ) {
			$tbrArr[] = " tbr.source_name like '%". $this->db->escape( $data['filter_name']) ."%'" ;
		}
		// dump( $tbsArr) ;
		if ( !empty( $tbsArr)) {
			$tb_summaryWhere = "WHERE" . join( " AND", $tbsArr) ;
		}
		if ( !empty( $tbrArr)) {
			$tb_resourceWhere = "AND" . join( " AND", $tbrArr) ;
		}

		$refundArr = array() ;
		$SQLCmd = "SELECT tbr.source_name, tbs.cnt, tbs.mcnt, tbs.sum_consume, tbs.sum_commission,
				ROUND( tbs.sum_commission * tbr.main_rate / 100 ) main_rate_calcul,
				ROUND( tbs.sum_commission * tbr.deduct_rate / 100 ) deduct_rate_calcul,
				ROUND( tbs.sum_commission * tbr.kmfun_rate / 100 ) kmfun_rate_calcul,
				ROUND( tbs.sum_commission * tbr.transfer_rate / 100 ) transfer_rate_calcul,
				ROUND( tbs.sum_commission * tbr.agency_rate / 100 ) agency_rate_calcul,
				ROUND( tbs.sum_commission * tbr.other_rate / 100 ) other_rate_calcul
			FROM tb_resource tbr
			LEFT JOIN ( SELECT guest_source, COUNT( guest_source ) cnt, SUM( consume ) sum_consume, SUM( commission ) sum_commission, sum( people) mcnt
				FROM tb_summary {$tb_summaryWhere} GROUP BY guest_source ) tbs ON tbr.source_name = tbs.guest_source
			WHERE tbs.cnt != '' {$tb_resourceWhere} order by tbr.source_name asc" ;
		// dump( $SQLCmd) ;
		$retArr = $this->db->query( $SQLCmd)->rows ;
		return $retArr ;
	}

	/**
	 * [exportRefundLists description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-04
	 */
	public function exportRefundLists ( $data = array()) {
		// dump( $data) ;
		$tbsArr = array() ;
		$tbrArr = array() ;
		$tb_summaryWhere = "" ;
		$tb_resourceWhere = "" ;

		if ( isset($data['filter_platform']) && !empty( $data['filter_platform']) ) {
			$tbsArr[] = " platform=". $this->db->escape( $data['filter_platform']) ;
		}
		if ( isset($data['filter_syear']) && !empty( $data['filter_syear']) ) {
			$tbsArr[] = " syear=". $this->db->escape( $data['filter_syear']) ;
		}
		if ( isset($data['filter_smonth']) && !empty( $data['filter_smonth']) ) {
			$tbsArr[] = " smonth=". $this->db->escape( $data['filter_smonth']) ;
		}
		if ( isset($data['filter_name']) && !empty( $data['filter_name']) ) {
			$tbrArr[] = " tbr.source_name like '%". $this->db->escape( $data['filter_name']) ."%'" ;
		}
		// dump( $tbsArr) ;
		if ( !empty( $tbsArr)) {
			$tb_summaryWhere = "WHERE" . join( " AND", $tbsArr) ;
		}
		if ( !empty( $tbrArr)) {
			$tb_resourceWhere = "AND" . join( " AND", $tbrArr) ;
		}

		$refundArr = array() ;
		// 領取方式 銀行戶名 銀行名稱 銀行代碼 帳號 備註
		// 領取方式 1:匯款;2:現金;
		$SQLCmd = "SELECT tbr.source_name, tbs.sum_commission,
				ROUND( tbs.sum_commission * tbr.main_rate / 100 ) main_rate_calcul,
				ROUND( tbs.sum_commission * tbr.deduct_rate / 100 ) deduct_rate_calcul,
				ROUND( tbs.sum_commission * tbr.kmfun_rate / 100 ) kmfun_rate_calcul,
				ROUND( tbs.sum_commission * tbr.transfer_rate / 100 ) transfer_rate_calcul,
				ROUND( tbs.sum_commission * tbr.agency_rate / 100 ) agency_rate_calcul,
				tbs.cnt, tbs.mcnt,
				CASE tbr.receive WHEN '1' THEN '匯款' WHEN '2' THEN '現金' END receive,
				tbr.account_name, tbr.bank_name, tbr.bank_branch, tbr.bank_code, tbr.bank_account, tbr.memo
			FROM tb_resource tbr
			LEFT JOIN ( SELECT guest_source, COUNT( guest_source ) cnt, SUM( commission ) sum_commission, sum( people) mcnt
				FROM tb_summary {$tb_summaryWhere} GROUP BY guest_source ) tbs ON tbr.source_name = tbs.guest_source
			WHERE tbs.cnt != '' {$tb_resourceWhere} order by tbr.source_name asc" ;
		// dump( $SQLCmd) ;
		$retArr = $this->db->query( $SQLCmd)->rows ;
		return $retArr ;
	}

	/**
	 * [importSummary description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-10-01
	 */
	public function importSummary( $data = array(), $platformInfo = "") {
		$cntImports = 0 ; // 成功匯入筆數
		$cntRows    = 0 ; // 本次匯入筆數
		$cntRepeat  = 0 ; // 重覆筆數
		$cntIllegal = 0 ; // 失敗筆數

		$existsArr = $this->makeExistsArr( $data, $platformInfo) ;
		// dump( $existsArr) ;
		foreach($data as $index => $rowCols) {
			// echo "$index\n" ;
			$rowCols['platform'] = $platformInfo['filter_platform'] ; // 昇恆昌
			$rowCols['syear']    = $platformInfo['filter_syear'] ;
			// echo "{$index}\n";
			$rowCols['G'] = ( $this->db->escape( $rowCols['G']) != "") ? $this->db->escape( $rowCols['G']) : 0 ;
			$rowCols['H'] = $this->excelReadToAbs( $rowCols['H']) ;
			$rowCols['I'] = $this->excelReadToAbs( $rowCols['I']) ;

			if ( !in_array( $this->buildCheckStr( $rowCols), $existsArr)) {
				$this->insTbSummary( $rowCols) ;
				$cntImports ++ ; // 成功匯入筆數
			} else {
				$cntRepeat ++ ; // 重覆筆數
			}
			$cntRows ++ ; // 本次匯入筆數
		}
		return array( "本次匯入筆數 : " .$cntRows. "筆", "成功匯入筆數 : ".$cntImports. "筆", "重覆筆數 : " .$cntRepeat. "筆", ) ;
	}

	/**
	 * [getResourceDetails description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-10-15
	 */
	public function getResourceDetails( $data = array()) {
		// dump( $data) ;
		$SQLCmd = "SELECT main_rate, deduct_rate, kmfun_rate, transfer_rate, agency_rate, other_rate FROM tb_resource
			WHERE source_name='{$this->db->escape( $data['filter_name'])}'" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->row ;
	}

	public function getDetailsList( $data = array()) {
		$column = array();
		if ( isset($data['filter_syear']) && $data['filter_syear'] != "") {
			$column[] = "syear='".$this->db->escape($data['filter_syear'])."'";
		}
		if ( isset($data['filter_smonth']) && $data['filter_smonth'] != "") {
			$column[] = "smonth='".$this->db->escape($data['filter_smonth'])."'";
		}
		if ( isset($data['filter_platform']) && $data['filter_platform'] != "") {
			$column[] = "platform='".$this->db->escape($data['filter_platform'])."'";
		}

		$SQLCmd = "SELECT platform, CONCAT(syear, '-', LPAD(smonth, 2, 0), '-', LPAD(sday, 2, 0)) occur_date,
				guest_source, consume, commission, people
			FROM tb_summary WHERE guest_source='{$data['filter_name']}' " ;

		if(count($column)) {
			$SQLCmd .= "AND " . implode(" AND ",$column);
		}
		$SQLCmd .= " ORDER BY platform ASC, syear ASC, smonth ASC, sday ASC" ;
		// dump( $SQLCmd) ;
		// dump( $this->db->query( $SQLCmd)->rows) ;
		return $this->db->query( $SQLCmd)->rows ;
	}

	public function dashboardTotal() {
		$SQLCmd = "SELECT platform, sum(consume) t_consume, sum(commission) t_commission, sum(appoint) t_appoint, sum( people) mcnt FROM tb_summary
				WHERE syear='".date('Y')."' GROUP BY platform ORDER BY platform ASC" ;
		return $this->db->query( $SQLCmd)->rows ;
	}


	/**
	 * [buildCheckStr description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-10-01
	 */
	private function buildCheckStr( $data = array()) {
		$tmp = trim( $this->db->escape( $data['A'])).trim( $this->db->escape( $data['B'])).trim( $this->db->escape( $data['C'])).
		trim( $this->db->escape( $data['D'])).trim( $this->db->escape( $data['E'])).trim( $this->db->escape( $data['F'])).
		trim( $this->db->escape( $data['G'])).trim( $this->db->escape( $data['H'])).trim( $this->db->escape( $data['I'])) ;

		return md5( $tmp) ;
	}

	/**
	 * [insTbSummary description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-10-01
	 */
	private function insTbSummary( $data = array()) {
		$insData = array() ;
		$insData[] = "platform='{$data['platform']}'" ;
		$insData[] = "syear={$data['syear']}" ;
		$insData[] = "smonth='".		trim( $this->db->escape( $data['A']))."'" ;
		$insData[] = "sday='".			trim( $this->db->escape( $data['B']))."'" ;
		$insData[] = "cardno='".		trim( $this->db->escape( $data['C']))."'";
		$insData[] = "makecard='".		trim( $this->db->escape( $data['D']))."'" ;
		$insData[] = "guest_source='".	trim( $this->db->escape( $data['E']))."'" ;
		$insData[] = "guest_name='".	trim( $this->db->escape( $data['F']))."'" ;
		$insData[] = "people='".		trim( $this->db->escape( $data['G']))."'" ;
		$insData[] = "consume='".		trim( $this->db->escape( $data['H']))."'" ;
		$insData[] = "commission='".	trim( $this->db->escape( $data['I']))."'" ;
		$insData[] = "appoint='".		trim( $this->db->escape( $data['J']))."'" ;

		$this->db->query( "INSERT INTO `tb_summary` SET ".implode(",",$insData));
	}

	/**
	 * [makeExistsArr description]
	 * @param   array      $data     [description]
	 * @param   string     $platform [description]
	 * @return  [type]               [description]
	 * @Another Angus
	 * @date    2019-10-01
	 */
	private function makeExistsArr( $data = array(), $platformInfo = "") {
		// 檢查是否有重覆匯入
		$sMonthArr = array() ;
		$sDayArr = array() ;
		foreach ($data as $index => $rowCols) {
			// 月份
			if ( !in_array( $this->db->escape( $rowCols['A']), $sMonthArr)) {
				$sMonthArr[] = $this->db->escape( $rowCols['A']) ;
			}
			// 日
			if ( !in_array( $this->db->escape( $rowCols['B']), $sDayArr)) {
				$sDayArr[] = $this->db->escape( $rowCols['B']) ;
			}
		}

		$whereArr = array() ;
		$whereArr[] = "platform='{$platformInfo['filter_platform']}'" ;
		$whereArr[] = "syear='{$platformInfo['filter_syear']}'" ;
		$whereArr[] = "smonth IN (".join(',', $sMonthArr).")" ;
		$whereArr[] = "sday IN (".join(',', $sDayArr).")" ;

		$SQLCmd = "SELECT CONCAT(smonth, sday, cardno, makecard, guest_source, guest_name, people, consume, commission) tmp FROM tb_summary WHERE " . join( " AND ", $whereArr) ;
		// dump( $SQLCmd) ;
		// exit() ;
		$query = $this->db->query( $SQLCmd) ;
		$md5Arr = array() ;
		if ( $query->rows) {
			// dump( $query->rows) ;
			foreach ($query->rows as $tmp) {
				$md5Arr[] = md5( $tmp['tmp']) ;
			}
		}

		return $md5Arr ;
	}

	/**
	 * [makeInsArr description]
	 * @param   array      $rowCol [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2019-02-18
	 */
	private function makeInsArr( $rowCols = array()) {

		// $data['H'] = $this->excelReadToAbs( $data['H']) ;
		// $data['I'] = $this->excelReadToAbs( $data['I']) ;

		$insArr['syear']        = $this->db->escape( trim( $rowCols['A'])) ;
		$insArr['smonth']       = $this->db->escape( trim( $rowCols['B'])) ;
		$insArr['sday']         = $this->db->escape( trim( $rowCols['C'])) ;

		$insArr['cardno']       = $this->db->escape( trim( $rowCols['D'])) ;
		$insArr['makecard']     = $this->db->escape( trim( $rowCols['E'])) ;
		$insArr['guest_source'] = $this->db->escape( trim( $rowCols['F'])) ;
		$insArr['guest_name']   = $this->db->escape( trim( $rowCols['G'])) ;

		$insArr['people']       = $this->db->escape( trim( $rowCols['H'])) ;
		$insArr['consume']      = $this->db->escape( trim( $rowCols['I'])) ;
		$insArr['commission']   = $this->db->escape( trim( $rowCols['J'])) ;
		$insArr['appoint']      = $this->db->escape( trim( $rowCols['K'])) ;

		return $insArr ;
	}

	/**
	 * [excelReadToAbs description]
	 * @param   integer    $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-10-09
	 */
	private function excelReadToAbs ( $data = 0) {
		if ( preg_match('/[\(\)]/', $this->db->escape( $data))) {
			return (0 - (int)preg_replace( '/[\(\)]/', '', $this->db->escape( $data))) ;
		} else {
			return $data ;
		}
	}



}