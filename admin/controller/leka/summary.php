<?php
class ControllerLekaSummary extends Controller {
	private $error = array() ;
	private $uploadFileName = "" ;
	private $func_path     = "leka/summary" ;

	/**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-28
	 */
	public function index() {
		$this->document->setTitle("退傭日誌管理 - 樂咖") ;

		$this->load->model( $this->func_path) ;

		$this->getList() ;
	}

	/**
	 * [importForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-02-17
	 */
	public function importForm() {
		// $this->document->setTitle("日誌匯入 - 樂咖") ;
		// dump( $this->request->post) ;
		// dump( $this->request->files) ;
		$this->load->model( $this->func_path) ;
		// $this->load->model('leka/resource') ;
		$this->load->language('leka/resource') ;

		if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm()) {

			$platform = isset( $this->request->post['modal_platform']) ? $this->request->post['modal_platform'] : "" ;
			$filter_data = array (
				'filter_syear'    => $this->request->post['modal_syear'],
				'filter_platform' => $this->request->post['modal_platform'],
			);

			$sheetData = $this->getExcelSheetData() ;
			$retMsgArr = $this->model_leka_summary->importSummary( $sheetData, $filter_data) ;
			$this->session->data['success'] = $retMsgArr ;
			$this->getList() ;
		}
	}

	/**
	 * [getList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-09-28
	 */
	protected function getList() {
		// dump( $this->request->get) ;
		$data = $this->init() ;
		$data['heading_title']   = "退傭日誌管理";
		$data['error_warning']   = "" ;
		$data['pagination']      = "" ;
		$data['results']         = "" ;
		$data['text_list']       = "列表" ;
		$data['column_action']   = "動作" ;
		$data['button_edit']     = "編輯" ;
		$data['text_no_results'] = $this->language->get('text_no_results') ;
		$data['token']           = $this->session->data['token'] ;

		if ( isset( $this->session->data['success'])) {
			$data['success'] = "匯入結果 : <br>" . join( "<br>", $this->session->data['success']) ;
			unset( $this->session->data['success']) ;
		} else {
			$data['success'] = '' ;
		}

		// 設定列表欄位 ---------------------------------------------------------------------------------
		$columnNames = array(
				"syear"        => '年',
				"smonth"       => '月',
				"sday"         => '日',
				"cardno"       => '卡號',
				"makecard"     => '製卡人員',
				"guest_source" => '客人來源',
				"guest_name"   => '客人名字',
				"people"       => '人數',
				"consume"      => '購買金額',
				"commission"   => '傭金',
			) ;
		// 設定input的長度
		$columnSize = array(
				"syear"        => '4',
				"smonth"       => '2',
				"sday"         => '2',
				"cardno"       => '10',
				"people"       => '3',
			) ;

		// 取得排序資訊
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'default';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		// 麵包屑 Start ---------------------------------------------------------------------------------
		$data['columnNames'] = $columnNames ;
		$data['columnSize']  = $columnSize ;
		$data['td_colspan']  = count( $columnNames) + 1 ;

		$data['breadcrumbs'] = array();
		$url = "" ;
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => "退傭日誌管理",
			'href' => $this->url->link('leka/summary', 'token=' . $this->session->data['token'] . $url, true)
		);
		// 麵包屑 End ---------------------------------------------------------------------------------

		// 欄位排序
		$url = '';
		if (isset($this->request->get['filter_syear'])) {
			$url .= '&filter_syear=' . $this->request->get['filter_syear'];
		}
		if (isset($this->request->get['filter_smonth'])) {
			$url .= '&filter_smonth=' . $this->request->get['filter_smonth'];
		}
		if (isset($this->request->get['filter_sday'])) {
			$url .= '&filter_sday=' . $this->request->get['filter_sday'];
		}
		if (isset($this->request->get['filter_platform'])) {
			$url .= '&filter_platform=' . $this->request->get['filter_platform'];
		}
		if (isset($this->request->get['searchFlag'])) {
			$url .= '&searchFlag=' . $this->request->get['searchFlag'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		$data['colSort']['cardno']       = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=cardno' . $url, true);
		$data['colSort']['makecard']     = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=makecard' . $url, true);
		$data['colSort']['guest_source'] = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=guest_source' . $url, true);
		$data['colSort']['guest_name']   = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=guest_name' . $url, true);
		$data['colSort']['people']       = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=people' . $url, true);
		$data['colSort']['consume']      = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=consume' . $url, true);
		$data['colSort']['commission']   = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=commission' . $url, true);

		// 查詢
		$filter_data = array (
			'filter_syear'        => $data["filter_syear"],
			'filter_smonth'       => $data["filter_smonth"],
			'filter_sday'         => $data["filter_sday"],
			'filter_platform'     => $data["filter_platform"],
			'filter_guest_source' => $data["filter_guest_source"],

			'sort'            => $sort,
			'order'           => $order,

			// 'start'           => ($data["page"] - 1) * $data["filter_limit"],
			// 'limit'           => $data["filter_limit"]
		);

		// 查詢條件
		if ( empty( $this->error) && !empty( $data['searchFlag'])) {
			$data['listRows'] = $this->model_leka_summary->getSummaryList( $filter_data) ;
			$data['rowCnt'] = count( $data['listRows']) ;
		} else {
			$data['error_warning'] = join( "<br>", $this->error) ;
			$data['listRows'] = "" ;
			$data['rowCnt'] = 0 ;
		}
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('leka/summary_list', $data));
	}

	/**
	 * [init description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-09-28
	 */
	protected function init() {
		$filterArr = array(
			"page"                => "頁碼",
			"filter_limit"        => "分頁數",

			"filter_syear"        => "年度",
			"filter_smonth"       => "月份",
			"filter_sday"         => "日期",
			"filter_platform"     => "平台",
			"filter_guest_source" => "來源",
			"searchFlag"          => "查詢",
		);

		foreach($filterArr as $col => $name){

			$data["{$col}"] = isset($this->request->get["{$col}"]) ? trim($this->request->get["{$col}"]) : "";
			if ( empty( $data["{$col}"]) && $col != "page" && $col != "filter_limit" && $col != "filter_sday") {
				$this->error[] = "{$name} 未選擇" ;
			}
			// if(isset($this->request->get["{$col}"]) && !empty($this->request->get["{$col}"])){
			//     $this->urlArr["default"] .=  "&{$col}=".$this->request->get["{$col}"];
			//     if($col!="page"){
			//         $this->urlArr["page"] .= "&{$col}=".$this->request->get["{$col}"];
			//     }
			// }
		}
		if ( empty($data['searchFlag'])) {
			$this->error = array() ;
		}
		return $data ;
	}

	/**
	 * [validateForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-02-17
	 */
	protected function validateForm() {
		// dump( $this->request->files) ;
		// $this->request->files[fileToUpload]
		// [fileToUpload] => Array
		// (
		// 	[name] => summary.xlsx
		// 	[type] => application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
		// 	[tmp_name] => /Applications/XAMPP/xamppfiles/temp/phpkhbuK0
		// 	[error] => 0
		// 	[size] => 73246
		// )
 		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'leka/summary')) {
			$this->error['warning'] = $this->language->get('error_permission') ;
		} else {
			if ( isset( $_FILES)) {
				$target_file = DIR_DOWNLOAD . $_FILES['fileToUpload']['name'] ;
				$this->uploadFileName = $target_file ;
				move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file) ;
			} else {
				$this->error['upload_file_exists'] = "未上傳檔案 or 檔案格式錯誤" ;
			}
			if ( $this->request->post['modal_syear'] == "") {
				$this->error['modal_syear'] = "未輸入年份" ;
			}
			if ( $this->request->post['modal_platform'] == "") {
				$this->error['modal_platform'] = "未輸入平台" ;
			}
		}
		return !$this->error;
	}


	/**
	 * [getExcelSheetData description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-10-01
	 */
	protected function getExcelSheetData () {
		ini_set("max_execution_time", 0) ;
		ini_set("memory_limit","2048M");
		include_once( DIR_SYSTEM . 'library/PHPExcel.php') ;
		$objPHPExcel = PHPExcel_IOFactory::load( $this->uploadFileName) ;
		$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true) ;

		$retSheetData = array() ;
		foreach ($sheetData as $iCnt => $row) {
			if ( $iCnt > 2) {
				if ( !empty( $row['A']) && !empty( $row['B'])) {
					$retSheetData[] = $row ;
				} else {
					break ;
				}
			}
		}

		return $retSheetData ;
	}
}
