<?php
/**
 * 總退傭表
 * @Another Angus
 * @date    2019-10-15
 */
class ControllerReportRefund extends Controller {
	private $columnNames = array(
				"source_name"          => '通路商名稱',
				"sum_consume"          => '總購物金',
				"main_rate_calcul"     => '通路實拿佣金',
				"kmfun_rate_calcul"    => '司機代駕傭金',
				"transfer_rate_calcul" => '金豐接送傭金',
				"other_rate_calcul"    => '其他傭金',
				"cnt"                  => '筆數',
				"mcnt"                 => '總人數',
			) ;

	/**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-30
	 */
	public function index() {
		$this->document->setTitle("總退傭表 - 樂咖") ;

		// $this->load->model('leka/resource') ;

		$this->getList() ;
	}

	/**
	 * [getList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-10-15
	 */
	protected function getList() {
		$data['heading_title']   = "總退傭表";
		$data['error_warning']   = "" ;
		$data['success']         = "" ;
		$data['pagination']      = "" ;
		$data['results']         = "" ;
		$data['text_list']       = "列表" ;
		$data['text_no_results'] = $this->language->get('text_no_results') ;
		$data['column_action']   = "檢視" ;
		$data['button_edit']     = "編輯" ;
		$data['token']           = $this->session->data['token'] ;

		$this->load->model('leka/resource') ;
		$this->load->model('leka/summary') ;

		// 整理列表搜尋條件
		// dump( $this->request->get) ;
		$filterArr = array(
			"page"            => "頁碼",
			"filter_limit"    => "分頁數",

			"filter_syear"    => "年度",
			"filter_smonth"   => "月份",
			"filter_name"     => "通路商名稱",
			"filter_platform" => "平台選擇",
		);
		foreach($filterArr as $col => $name){
			$data["{$col}"] = isset($this->request->get["{$col}"]) ? trim($this->request->get["{$col}"]) : "";
		}

		// 設定列表欄位 ---------------------------------------------------------------------------------
		$data['columnNames'] = $this->columnNames ;
		$data['td_colspan']  = count( $this->columnNames) + 2 ;


		$data['breadcrumbs'] = array();
		$url = "" ;
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => "總退傭表",
			'href' => $this->url->link('report/refund', 'token=' . $this->session->data['token'] . $url, true)
		);

		if ( !empty( $data["filter_platform"]) && !empty( $data["filter_syear"]) && !empty( $data["filter_smonth"])) {
			$data['export'] = true ;
		} else {
			$data['export'] = false ;
		}

		// 查詢
		$filter_data = array (
			'filter_syear'    => $data["filter_syear"],
			'filter_smonth'   => $data["filter_smonth"],
			'filter_name'     => $data["filter_name"],
			'filter_platform' => $data["filter_platform"],

			'start'           => ($data["page"] - 1) * $data["filter_limit"],
			'limit'           => $data["filter_limit"]
		);

		$refundRoleArr = $this->model_leka_summary->getRefundLists( $filter_data) ;
		// dump( count( $refundRoleArr)) ;
		$data['listRows'] = $refundRoleArr ;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/all_refund_report', $data));
	}

	/**
	 * [ajaxGetDetails description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-10-15
	 */
	public function ajaxGetDetails() {
		$this->load->model('leka/summary') ;
		$platformName = array( "1" => "昇恆昌", "2" => "金坊") ;
		// dump( $this->request->get) ;
		$retInfo = array() ;

		$filter_data = array(
			"filter_name"     => isset( $this->request->get['filter_name']) ? $this->request->get['filter_name'] : "",
			"filter_syear"    => isset( $this->request->get['filter_syear']) ? $this->request->get['filter_syear'] : "",
			"filter_smonth"   => isset( $this->request->get['filter_smonth']) ? $this->request->get['filter_smonth'] : "",
			"filter_platform" => isset( $this->request->get['filter_platform']) ? $this->request->get['filter_platform'] : "",
		) ;

		// 取得比例數
		$sourceInfo = $this->model_leka_summary->getResourceDetails( $filter_data) ;
		// dump( $sourceInfo) ;
		$htmlStr = "" ;
		$htmlStr .= "<tr class='text-danger'><td class='text-center'>{$sourceInfo['main_rate']}</td>" ;
		$htmlStr .= "<td class='text-center'>{$sourceInfo['kmfun_rate']}</td>" ;
		$htmlStr .= "<td class='text-center'>{$sourceInfo['transfer_rate']}</td>" ;
		$htmlStr .= "<td class='text-center'>{$sourceInfo['other_rate']}</td></tr>" ;
		$json[] = $htmlStr ;

		// 取明細
		$retRows = $this->model_leka_summary->getDetailsList( $filter_data) ;
		$htmlStr = "" ;
		foreach ($retRows as $iCnt => $row) {
			$htmlStr .= "<tr><td class='text-center'>{$platformName[$row['platform']]}</td>" ;
			$htmlStr .= "<td class='text-center'>{$row['occur_date']}</td>" ;
			$htmlStr .= "<td class='text-center'>{$row['guest_source']}</td>" ;
			$htmlStr .= "<td class='text-center'>". number_format( $row['consume']) . "</td>" ;
			$htmlStr .= "<td class='text-center'>". number_format( $row['commission']) . "</td>" ;
			$htmlStr .= "<td class='text-center'>{$row['people']}</td></tr>" ;
		}
		$json[] = $htmlStr ;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * [exportExcel description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-04
	 */
	public function exportExcel() {
		// dump( $this->request->get) ;

		$this->load->model('leka/summary') ;
		// 平台名稱
		$platformArr = array(
			"1" => "昇恆昌",
			"2" => "金坊",
		) ;

		// 查詢欄位名稱
		$filterArr = array(
			"filter_syear"    => "年度",
			"filter_smonth"   => "月份",
			"filter_name"     => "通路商名稱",
			"filter_platform" => "平台選擇",
		);
		foreach($filterArr as $col => $name){
			$data["{$col}"] = isset($this->request->get["{$col}"]) ? trim($this->request->get["{$col}"]) : "";
		}

		$filter_data = array (
			'filter_syear'    => $data["filter_syear"],
			'filter_smonth'   => $data["filter_smonth"],
			'filter_name'     => $data["filter_name"],
			'filter_platform' => $data["filter_platform"],
		);

		// 增加欄位
		// 領取方式 銀行戶名 銀行名稱 銀行代碼 帳號 備註
		// account_name, tbr.bank_name, tbr.bank_branch, tbr.bank_code, tbr.bank_account, tbr.memo
		$this->columnNames['receive']      = "領取方式" ;
		$this->columnNames['account_name'] = "銀行戶名" ;
		$this->columnNames['bank_name']    = "銀行名稱" ;
		$this->columnNames['bank_branch']  = "分行代碼" ;
		$this->columnNames['bank_code']    = "銀行代碼" ;
		$this->columnNames['bank_account'] = "帳號" ;
		$this->columnNames['memo']         = "備註" ;


		$platformName = !empty( $data["filter_platform"]) ? $platformArr[$data["filter_platform"]] : "" ;
		$refundRoleArr = $this->model_leka_summary->exportRefundLists( $filter_data) ;

		// dump( $refundRoleArr) ;
		// exit() ;
		// header("Content-type:application/vnd.ms-excel");
		// header("Content-type: text/x-csv");
		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition:filename=總退傭表_{$platformName}_{$data["filter_syear"]}_{$data["filter_smonth"]}.xls");

		// 欄位產出
		$content = join( "\t", $this->columnNames) ;
		$content = mb_convert_encoding($content , "Big5" , "UTF-8") ;
		echo $content . "\n" ;
		foreach ($refundRoleArr as $iCnt => $row) {
			$row['bank_account'] = "'". $row['bank_account'] ;
			$content = "" ;
			// foreach ($this->columnNames as $keyName => $value) {
			// 	$content .= $row[$keyName]."\t" ;
			// }

			// dump( $content) ;
			$content = join( "\t", $row) ;
			$content = mb_convert_encoding($content , "Big5" , "UTF-8") ;
			echo $content . "\n" ;
		}
		exit;
	}
}
