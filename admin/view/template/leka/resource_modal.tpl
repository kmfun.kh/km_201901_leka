<!-- 出貨回報 -->
<div class="modal fade" id="actModal_1" tabindex="-1" role="dialog" aria-hidden="true">
</div>

<!-- 修改 -->
<form class="form-horizontal" id="formModal" method="post" action="">
<div class="modal fade" id="modalResourceDetails" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">客人來源管理 修改</h3>
			</div>
			<div class="modal-body">
				<div style="margin-bottom:10px;">
					<h4><i class="fa fa-chevron-right" aria-hidden="true"></i>
					<span id="detail-title"></span>
					</h4>
				</div>
				<input type="hidden" name="rDetails_idx">
				<div class="row form-group">
					<label class="col-sm-2 control-label">帳號</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="rDetails_account" placeholder="帳號" />
					</div>
					<label class="col-sm-2 control-label">領取方式</label>
					<div class="col-sm-4">
						<select name="rDetails_receive" class="form-control">
							<option>請選擇領取方式</option>
							<option value="1">匯款</option>
							<option value="2">現金</option>
						</select>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 control-label">密碼</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="rDetails_password" placeholder="密碼" />
					</div>
					<label class="col-sm-2 control-label">確認密碼</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="rDetails_repassword" placeholder="確認密碼" />
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 control-label">email</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="rDetails_email" placeholder="email" />
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 control-label align-items-center">銀行戶名</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="rDetails_account_name" placeholder="銀行戶名" />
					</div>
					<label class="col-sm-2 control-label">銀行代碼</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="rDetails_bank_code" placeholder="銀行代碼" />
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 control-label">銀行名稱</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="rDetails_bank_name" placeholder="銀行名稱" />
					</div>
					<label class="col-sm-2 control-label">分行</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="rDetails_bank_branch" placeholder="分行" />
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 control-label">銀行帳號</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="rDetails_bank_account" placeholder="銀行帳號" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="sendResource();">保存</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">關閉</button>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">

</script>

<!-- 新增 -->
<form class="form-horizontal" id="formNewModal" method="post" action="">
<div class="modal fade" id="modalNewResourceDetails" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">客人來源管理 新增</h3>
			</div>
			<div class="modal-body">
				<div class="row form-group">
					<label class="col-sm-2 control-label">通路商名稱</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="nDetails_source_name" placeholder="通路商名稱" />
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 control-label">帳號</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="nDetails_account" placeholder="帳號" />
					</div>
					<label class="col-sm-2 control-label">領取方式</label>
					<div class="col-sm-4">
						<select name="nDetails_receive" class="form-control">
							<option>請選擇領取方式</option>
							<option value="1">匯款</option>
							<option value="2">現金</option>
						</select>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 control-label">密碼</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="nDetails_password" placeholder="密碼" />
					</div>
					<label class="col-sm-2 control-label">確認密碼</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="nDetails_repassword" placeholder="確認密碼" />
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 control-label">email</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="nDetails_email" placeholder="email" />
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 control-label align-items-center">銀行戶名</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="nDetails_account_name" placeholder="銀行戶名" />
					</div>
					<label class="col-sm-2 control-label">銀行代碼</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="nDetails_bank_code" placeholder="銀行代碼" />
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 control-label">銀行名稱</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="nDetails_bank_name" placeholder="銀行名稱" />
					</div>
					<label class="col-sm-2 control-label">分行</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="nDetails_bank_branch" placeholder="分行" />
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 control-label">銀行帳號</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="nDetails_bank_account" placeholder="銀行帳號" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="sendNewResource();">保存</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">關閉</button>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">

</script>