<form method="post" action="" enctype="multipart/form-data" id="form-import-modal">
<div class="modal fade" id="modalImportSummary" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">匯入 退傭日誌</h3>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="control-label">平台選擇</label>
						<select name="modal_platform" class="form-control">
							<option value="">請選擇</option>
							<option value="1" <?php if($filter_platform == "1"):?>selected<?php endif;?>>昇恆昌</option>
							<option value="2" <?php if($filter_platform == "2"):?>selected<?php endif;?>>金坊</option>
						</select>
				</div>
				<div class="form-group">
					<label class="control-label">年度選擇</label>
						<select name="modal_syear" class="form-control">
							<option value="">請選擇</option>
							<option value="2019">2019</option>
						</select>
				</div>
				<div class="form-group">
					<label class="control-label">上傳檔案</label>
					<input type="file" class="form-control" id="fileToUpload" name="fileToUpload" >
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="bt_modal_upload">上傳</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">關閉</button>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$('#bt_modal_upload').click( function () {
	console.log( "bt_modal_upload") ;
	var msg = "" ;
	console.log( $("select[name='modal_platform']").val());
	if ( $("select[name='modal_platform']").val() == "") {
		msg += "平台未選擇\n" ;
		console.log( msg);
	}
	if ( $("select[name='modal_syear']").val() == "") {
		msg += "年度未選擇\n" ;
		console.log( msg);
	}
	if ( $('[name="fileToUpload"]').val() == "") {
		msg += "檔案未選擇\n" ;
		console.log( msg);
	}
	if ( msg != "") {
		alert( msg) ;
		return false ;
	} else {
		var url = '?route=leka/summary/importForm&token='+getURLVar('token');
		console.log( url) ;
		$('#form-import-modal').attr('action', url).submit();
	}
});
</script>