<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />

</head>
<body>
	<div id="container">
		<div id="content">
				<div class="container-fluid">
					<h1><?php echo $heading_title; ?></h1>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-list"></i> <?=$text_title?></h3>
						</div>
						<?php if ($error_warning) { ?>
						<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						</div>
						<?php } ?>
						<?php if ($success) { ?>
						<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						</div>
						<?php } ?>
						<div class="panel-body">
							<form class="form-horizontal" id="form" method="post" action="">
<input type="hidden" name="source_id" value="<?=$infoArr['idx']?>">
<div class="form-group required">
	<label class="col-sm-2 control-label">帳號</label>
	<div class="col-sm-10">
		<input type="text" name="account" value="<?=$infoArr['account']?>" placeholder="帳號" id="input-account" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-email">email</label>
	<div class="col-sm-10">
		<input type="text" name="email" value="<?=$infoArr['email']?>" placeholder="Email" id="input-email" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-password">密碼</label>
	<div class="col-sm-10">
		<input type="password" name="password" value="<?=$infoArr['password']?>" placeholder="密碼" id="input-password" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-repassword">確認密碼</label>
	<div class="col-sm-10">
		<input type="password" name="repassword" value="<?=$infoArr['repassword']?>" placeholder="確認密碼" id="input-repassword" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-receive">領取方式</label>
	<div class="col-sm-10">
		<?php
			$seled1 = ($infoArr['receive'] == '1') ? "selected" : "" ;
			$seled2 = ($infoArr['receive'] == '2') ? "selected" : "" ;
		?>
		<select name="receive">
			<option value="">-- 請選擇 --</option>
			<option value="1" <?=$seled1?>>匯款</option>
			<option value="2" <?=$seled2?>>現金</option>
		</select>
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-account_name">銀行戶名</label>
	<div class="col-sm-10">
		<input type="text" name="account_name" value="<?=$infoArr['account_name']?>" placeholder="銀行戶名" id="input-account_name" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-bank_name">銀行名稱</label>
	<div class="col-sm-10">
		<input type="text" name="bank_name" value="<?=$infoArr['bank_name']?>" placeholder="銀行名稱" id="input-bank_name" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-bank_branch">分行</label>
	<div class="col-sm-10">
		<input type="text" name="bank_branch" value="<?=$infoArr['bank_branch']?>" placeholder="分行" id="input-bank_branch" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-bank_code">銀行代碼</label>
	<div class="col-sm-10">
		<input type="text" name="bank_code" value="<?=$infoArr['bank_code']?>" placeholder="銀行代碼" id="input-bank_code" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-bank_account">銀行帳號</label>
	<div class="col-sm-10">
		<input type="text" name="bank_account" value="<?=$infoArr['bank_account']?>" placeholder="銀行帳號" id="input-bank_account" class="form-control" />
	</div>
</div>
								<button type="submit" id="close" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
							</form>
						</div>
					</div>
				</div>
		</div>
	</div>
<script type="text/javascript">
$('#close').click(function(event) {
	// parent.jQuery.fancybox.getInstance().close();
});

</script>
</body>

<footer id="footer">活動咖 &copy; 2017-2019 All Rights Reserved</footer></div>
</body></html>
