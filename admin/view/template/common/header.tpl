<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

	<link rel="stylesheet" type="text/css" href="view/javascript/jquery/jquery-ui/jquery-ui.min.css" />
	<link rel="stylesheet" type="text/css" href="view/stylesheet/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="view/javascript/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />

	<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="view/javascript/jquery/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="view/javascript/jquery/datetimepicker/moment.js"></script>
    <script type="text/javascript" src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
	<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>


<?php foreach ($styles as $style) { ?>
<link type="text/css" href="<?php echo $style['href']; ?>" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
</head>
<body>
<div id="container">
    <header id="header" class="navbar navbar-static-top login">
        <div class="navbar-header">
            <?php if ($logged) { ?>
            <a type="button" id="button-menu" class="pull-left">
                <?php if($col_left=='active'){?>
                <i class="fa fa-dedent fa-lg"></i>
                <?php }else{?>
                <i class="fa fa-indent fa-lg"></i>
                <?php }?>
            </a>
            <?php } ?>
            <a href="<?php echo $home; ?>" class="navbar-brand">
                <img src="view/image/logo.png" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" <?php echo $navbar_logo_hieght; ?>/></a>
        </div>
        <?php if ($logged) { ?>
        <ul class="nav pull-right">
            <li><a href="<?php echo $logout; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_logout; ?></span> <i class="fa fa-sign-out fa-lg"></i></a></li>
        </ul>
        <?php } ?>
    </header>
