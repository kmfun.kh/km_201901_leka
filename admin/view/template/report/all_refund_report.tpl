<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">平台選擇</label>
								<select name="filter_platform" class="form-control">
									<option value="">請選擇</option>
									<option value="1" <?php if( @$filter_platform == "1"):?>selected<?php endif;?>>昇恆昌</option>
									<option value="2" <?php if( @$filter_platform == "2"):?>selected<?php endif;?>>金坊</option>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label">通路商名稱</label>
								<input type="text" name="filter_name" value="<?=@$filter_name?>" class="form-control" />
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">年度</label>
								<select name="filter_syear" class="form-control">
									<option value="">請選擇</option>
									<option value="2019" <?php if( @$filter_syear == "2019"):?>selected<?php endif;?>>2019</option>
									<option value="2020" <?php if( @$filter_syear == "2020"):?>selected<?php endif;?>>2020</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">月份</label>
								<select name="filter_smonth" class="form-control">
									<option value="">請選擇</option>
									<option value="1"  <?php if( @$filter_smonth == "1"):?>selected<?php endif;?>>一月</option>
									<option value="2"  <?php if( @$filter_smonth == "2"):?>selected<?php endif;?>>二月</option>
									<option value="3"  <?php if( @$filter_smonth == "3"):?>selected<?php endif;?>>三月</option>
									<option value="4"  <?php if( @$filter_smonth == "4"):?>selected<?php endif;?>>四月</option>
									<option value="5"  <?php if( @$filter_smonth == "5"):?>selected<?php endif;?>>五月</option>
									<option value="6"  <?php if( @$filter_smonth == "6"):?>selected<?php endif;?>>六月</option>
									<option value="7"  <?php if( @$filter_smonth == "7"):?>selected<?php endif;?>>七月</option>
									<option value="8"  <?php if( @$filter_smonth == "8"):?>selected<?php endif;?>>八月</option>
									<option value="9"  <?php if( @$filter_smonth == "9"):?>selected<?php endif;?>>九月</option>
									<option value="10" <?php if( @$filter_smonth == "10"):?>selected<?php endif;?>>十月</option>
									<option value="11" <?php if( @$filter_smonth == "11"):?>selected<?php endif;?>>十一月</option>
									<option value="12" <?php if( @$filter_smonth == "12"):?>selected<?php endif;?>>十二月</option>
								</select>
							</div>
							<div class="form-group">
								<button type="button" id="button-clear" class="btn btn-danger pull-right" style="margin-left:5px;"><i class="fa fa-eraser"></i> 清除</button>
								<?php if ( $export) : ?>
								<button type="button" id="button-export" class="btn btn-success pull-right" style="margin-left:5px;"><i class="fa fa-file-excel-o"></i> 匯出</button>
								<?php endif ; ?>
								<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> 查詢</button>
							</div>
						</div>
						<div class="col-sm-4">
							&nbsp;
						</div>
						<div class="col-sm-4">
							&nbsp;
						</div>
					</div>
				</div>
				<form method="post" action="" enctype="multipart/form-data" id="form-order">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<!--
									<td class="text-center">
										<a href="<?php echo $sort_number; ?>" class="<?php echo ($sort=='order_number'?strtolower($order):''); ?>"><?php echo $column_order_number; ?></a>
									</td>
									-->
									<td width="1%" class="text-center">
										<input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
									</td>
									<?php foreach ($columnNames as $colKey => $colName) : ?>
									<td class="text-center">
										<?php if ( isset( $colSort[$colKey])) : ?>
											<?php if ( $sort == $colKey) : ?>
											<a href="<?=$colSort[$colKey]?>" class="<?php echo strtolower($order); ?>"><?=$colName?></a>
											<?php else : ?>
											<a href="<?=$colSort[$colKey]?>"><?=$colName?></a>
											<?php endif ; ?>
										<?php else : ?>
										<?=$colName?>
										<?php endif ; ?>
									</td>
									<?php endforeach ; ?>
									<td class="text-center"><?php echo $column_action; ?></td>
								</tr>
							</thead>
							<tbody>
								<?php if ($listRows) { ?>
								<?php foreach ($listRows as $row) { ?>
								<tr>
									<td class="text-center"><input type="checkbox" name="selected[]" value="<?php echo $row['source_name']; ?>" /></td>
									<?php foreach ($columnNames as $colKey => $colName) : ?>
										<?php if ( $colKey == "source_name") : ?>
									<td class="text-center"><?=$row[$colKey]?></td>
										<?php else : ?>
									<td class="text-center"><?=number_format($row[$colKey])?></td>
										<?php endif ; ?>
									<?php endforeach ; ?>
									<td class="text-center">
										<!--<a data-fancybox data-type="iframe" href="javascript:;" title="<?php echo $button_edit; ?>" data-toggle="tooltip"
											data-src="?route=loka/resource/ajax&token=<?=$token?>&idx=<?=$row['idx']; ?>"
											class="btn btn-primary editRow">
											<i class="fa fa-eye"></i>
										</a>-->
										<button type="button" class="btn btn-primary" data-toggle="tooltip" title="檢視" onclick="openModal('<?php echo $row['source_name']; ?>')"><i class="fa fa-eye"></i></button>
									</td>
								</tr>
								<?php } ?>
								<?php } else { ?>
								<tr>
									<td class="text-center" colspan="<?=$td_colspan?>"><?php echo $text_no_results; ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</form>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
					<div class="col-sm-6 text-right"><?php echo $results; ?></div>
				</div>
			</div>
		</div>
		<?php include_once("all_refund_modal.tpl");?>
	</div>
	<script src="view/javascript/modal_refund.js?1" type="text/javascript"></script>
	<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
	<style type="text/css">
	</style>
	<script type="text/javascript">
	$('.date').datetimepicker({
			pickTime: false
		});
	// 搜尋
	$('#button-filter').on('click', function() {
		var url = '?route=report/refund&token='+getURLVar('token');
		url = filterSend( url);
		location = url;
	});
	function filterSend( url){
		// url = '?route=report/refund&token='+getURLVar('token');
		var filter = ['syear', 'smonth', 'name', 'platform'];
		for(var i=0;i<filter.length;i++) {
			var target = $('[name="filter_'+filter[i]+'"]').val();
			if(target != ''){
				url += '&filter_'+filter[i]+'=' + encodeURIComponent(target);
			}
		}
		return url ;
	}
	// 匯出
	$('#button-export').on('click', function() {
		var url = '?route=report/refund/exportExcel&token='+getURLVar('token');
		url = filterSend( url);
		window.open( url , "export") ;
	});
	// 清除
	$("#button-clear").click(function(){
			console.log('button-clear');
			$('[name="search_date"]').val('');
			$('[name="search_date"]').val('');
	});

	</script>
</div>
<?php echo $footer; ?>