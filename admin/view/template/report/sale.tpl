<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td class="text-center"><?php echo $column_month; ?></td>
                                        <td class="text-center"><?php echo $column_sale; ?></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($results)){
                                        foreach($results as $month => $total){
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $month; ?></td>
                                        <td class="text-center">
                                            <?php 
                                                if(!empty($total)){
                                                    echo '<a href="'.$detail.'&month='.$month.'">'.$total.'</a>';
                                                }
                                                else{
                                                    echo $total;
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php }}else{?>
                                    <tr>
                                        <td class="text-center" colspan="2"><?php echo $text_no_results;?></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-right"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i> <i class="caret"></i></a>
                            <ul id="range" class="dropdown-menu dropdown-menu-right">
                                <?php foreach($yearArr as $k => $v){?>
                                <li <?php if($k==0){?>class="active"<?php }?>><a href="<?php echo $v; ?>"><?php echo $v; ?></a></li>
                                <?php }?>
                            </ul>
                        </div>
                        <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> <?php echo $heading_title; ?></h3>
                    </div>
                    <div class="panel-body">
                        <div id="chart-sale" style="width: 100%; height: 260px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="view/javascript/jquery/flot/jquery.flot.js"></script> 
<script type="text/javascript" src="view/javascript/jquery/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript">
$('#range a').on('click', function(e) {
	e.preventDefault();
	
	$(this).parent().parent().find('li').removeClass('active');
	
	$(this).parent().addClass('active');
	
	$.ajax({
		type: 'get',
		url: 'index.php?route=report/sale&token=<?php echo $token; ?>&range=' + $(this).attr('href'),
		dataType: 'json',
		success: function(json) {
            if (typeof json['order'] == 'undefined') { return false; }
			var option = {	
				shadowSize: 0,
				colors: ['#9FD5F1'],
				bars: {
					show: true,
					fill: true,
					lineWidth: 1
				},
				grid: {
					backgroundColor: '#FFFFFF',
					hoverable: true
				},
				points: {
					show: false
				},
				xaxis: {
					show: true,
            		ticks: json['xaxis']
				}
			}
			
			$.plot('#chart-sale', [json['order']], option);	
					
			$('#chart-sale').bind('plothover', function(event, pos, item) {
				//console.log(item);
                $('.tooltip').remove();
			  
				if (item) {
                    var txt = item.datapoint[0]+'月 '+item.datapoint[1]+' 筆';
					$('<div id="tooltip" class="tooltip top in"><div class="tooltip-arrow"></div><div class="tooltip-inner">' + txt + '</div></div>').prependTo('body');
					
					$('#tooltip').css({
						position: 'absolute',
						left: item.pageX - ($('#tooltip').outerWidth() / 2),
						top: item.pageY - $('#tooltip').outerHeight(),
						pointer: 'cusror'
					}).fadeIn('slow');	
					
					$('#chart-sale').css('cursor', 'pointer');		
			  	} else {
					$('#chart-sale').css('cursor', 'auto');
				}
			});
		},
        error: function(xhr, ajaxOptions, thrownError) {
           alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});

$('#range .active a').trigger('click');
</script> 
</div>
<?php echo $footer; ?>