<div id="albumImage" class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">封面設定</h4>
        </div>
        <div class="modal-body">
            <?php if(count($images)){?>
            <div class="row">
                <?php foreach($images as $id => $image) { ?>
                <div class="col-sm-3 col-xs-6 text-center">
                    <a href="javascript:void(0);" class="thumbnail"><img src="<?php echo $image; ?>" /></a>
                    <input type="hidden" name="mid[]" value="<?php echo $id; ?>" />
                </div>
                <?php } ?>
            </div>
            <?php }else{?>
            <div class="text-center" style="height:100px;"><h2><?php echo $err_msg;?></h2></div>
            <?php }?>
        </div>
    </div>
</div>
<script type="text/javascript">
$('a.thumbnail').on('click', function(e) {
	e.preventDefault();
    $('#img-change').find('img').attr('src', $(this).find('img').attr('src'));
	$('#img-change').parent().find('input').val($(this).parent().find('input').val());
	$('#modal-image').modal('hide');
});
</script>