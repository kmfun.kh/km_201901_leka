<div class="modal fade" id="article_modal" tabindex="-1" role="dialog" aria-labelledby="family-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="取消"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body form-horizontal">
                <table id="modal_table" class="table table-bordered">
                    <thead>
                        <tr>
                            <td>No.</td>
                            <td>日期</td>
                            <td>裝置</td>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>