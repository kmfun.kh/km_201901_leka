<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="javascript:void(0);" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary" onclick="checkSubmit();"><i class="fa fa-save"></i></a>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-article" class="form-horizontal">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-title"><?php echo $columnNames["title"]; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="title" value="<?php echo $title; ?>" id="input-title" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-summary"><?php echo $columnNames["summary"]; ?></label>
                        <div class="col-sm-10">
                            <textarea name="summary" id="input-summary" class="form-control"><?php echo $summary; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo $columnNames["imgurl"]; ?></label>
                        <div class="col-sm-10">
                            <a href="javascript:(0);" id="thumb-image" class="img-thumbnail">
                                <img src="<?php echo $imgurl; ?>" data-placeholder="<?php echo $placeholder; ?>" alt="" title="" width="100" height="100" />
                            </a>
                            <input type="file" name="imgurl" value="" id="input-image" style="display:none;" onchange="imageChange(this);"/>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo $columnNames["tags"]; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="tags" value="<?php echo $tags; ?>" data-role="tagsinput"  class="form-control" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo $columnNames["pagelink"]; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="pagelink" value="<?php echo $pagelink; ?>" class="form-control" placeholder="https://www.eventpal.com.tw"/>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo $columnNames["startdate"]; ?></label>
                        <div class="col-sm-4">
                            <div class="input-group date" id="sdate">
                                <input type="text" name="startdate" value="<?php echo $startdate; ?>" data-date-format="YYYY-MM-DD HH:mm:ss" class="form-control" />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo $columnNames["enddate"]; ?></label>
                        <div class="col-sm-4">
                            <div class="input-group date" id="edate">
                                <input type="text" name="enddate" value="<?php echo $enddate; ?>" data-date-format="YYYY-MM-DD HH:mm:ss" class="form-control" />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo $columnNames["flag"]; ?></label>
                        <div class="col-sm-10">
                            <label class="radio-inline"><input type="radio" name="flag" value="0" <?php if($flag==0){?>checked<?php }?>>否</label>
                            <label class="radio-inline"><input type="radio" name="flag" value="1" <?php if($flag==1){?>checked<?php }?>>是</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo $columnNames["sort"]; ?></label>
                        <div class="col-sm-4">
                            <input type="text" name="sort" value="<?php echo $sort; ?>" class="form-control" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
  </div>
</div>
<?php echo $footer; ?> 
<link href="view/stylesheet/bootstrap-tagsinput.css" type="text/css" rel="stylesheet" />
<script src="view/javascript/bootstrap-tagsinput.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#sdate,#edate').datetimepicker();
    $('#form-article').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
            e.preventDefault();
            return false;
        }
    });
});
function imageChange(obj){
    var ext = $(obj).val().split('.').pop().toLowerCase();
    if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
        $(obj).val('');
        alert('上傳圖檔格式錯誤！\n\n 請上傳 gif,png,jpg,jpeg 檔案');
        return false;
    }
    if (obj.files && obj.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#thumb-image img').attr('src', e.target.result);
        }
        reader.readAsDataURL(obj.files[0]);
    }
}
function checkSubmit(){
    var err = new Array();
    
    if(err.length>0){
        alert(err.join('\n'));
        return false;
    }
    else{
        $('#form-article').submit();
    }
}
</script>