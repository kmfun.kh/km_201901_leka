function openModal( name) {
	console.log( "openModal") ;
	var url = '?route=report/refund/ajaxGetDetails&token='+getURLVar('token');

	$('#detail-title').html( name) ;
	var filter = ['syear', 'smonth', 'platform'] ;
	for(var i=0;i<filter.length;i++) {
		var target = $('[name="filter_'+filter[i]+'"]').val() ;
		if(target != '') {
			url += '&filter_'+filter[i]+'=' + encodeURIComponent(target) ;
		}
	}
	url += "&filter_name=" + name ;
	console.log( url);

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(resp) {
			console.log( resp);
			$("#modalRate").find("tbody").html(resp[0]);
			$("#modalList").find("tbody").html(resp[1]);
		}
	});

	$("#modalDetails").modal('show') ;
}
