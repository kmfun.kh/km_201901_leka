/**
 * [openNewModal description]
 * @return  {[type]}   [description]
 * @Another Angus
 * @date    2019-10-01
 */
function openNewModal() {
	console.log("openNewModal") ;
	// formNewModal
	$("#modalNewResourceDetails").modal('show') ;
}
function sendNewResource() {
	console.log( 'sendNewResource');
	var msg     = "" ;
	var filter  = ['account', 'receive', 'password', 'repassword', 'email'];
	var pwStr   = $('[name="nDetails_password"]').val() ;
	var repwStr = $('[name="nDetails_repassword"]').val() ;

	console.log( pwStr);
	console.log( repwStr);
	if ( $('[name="nDetails_source_name"]').val() == '') {
		msg += "通路商名稱 未填寫\n" ;
	}
	if ( $('[name="nDetails_account"]').val() == '') {
		msg += "帳號 未填寫\n" ;
	}
	if ( pwStr == '') {
		msg += "密碼 未填寫\n" ;
	}
	if ( repwStr == '') {
		msg += "確認密碼 未填寫\n" ;
	}
	if ( pwStr != '' && repwStr != '') {
		if ( pwStr != repwStr) {
			msg += "密碼 及 確認密碼 不同\n" ;
		}
	}

	console.log( msg) ;
	if ( msg == "") {
		var url   = '?route=leka/resource/addForm&token=' + getURLVar('token') ;
		$('#formNewModal').attr('action', url).submit();
	} else {
		alert( msg) ;
	}
}


/**
 * [openModal description]
 * @param   {String}   id [description]
 * @return  {[type]}      [description]
 * @Another Angus
 * @date    2019-10-01
 */
function openModal( id = "") {
    console.log('id : ' + id);
    var colName = ['idx', 'account', 'account_name', 'receive', 'bank_account', 'bank_branch', 'bank_code', 'bank_name', 'email' ];
	$.ajax({
		url: '?route=leka/resource/modalResourceDetails&token=' +  getURLVar('token') +'&idx='+ id,
		dataType: 'json',
		success: function(resp) {
			// for(var k in resp) {
			// 	console.log(k, resp[k]);
			// }
			var k = "receive" ;
			console.log( k, resp[k]);
			$('#detail-title').html( " 通路商名稱 : " + resp.source_name) ;
			for(var i=0; i < colName.length; i++) {
				if ( colName[i] == "receive" && resp[colName[i]] == "") {
					console.log( k, resp[k]);
				} else {
					$('[name="rDetails_'+colName[i]+'"]').val( resp[colName[i]]) ;
				}
			}
			$('[name="rDetails_receive"]')
		}
	});

	$("#modalResourceDetails").modal('show') ;
    console.log("modalResourceDetails");
}

/**
 * [sendResource description]
 * @return  {[type]}   [description]
 * @Another Angus
 * @date    2019-10-01
 */
function sendResource() {
	console.log( 'sendResource');
	var url   = '?route=leka/resource/editForm&token=' + getURLVar('token') ;
	$('#formModal').attr('action', url).submit();
}
