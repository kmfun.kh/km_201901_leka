<?php
error_reporting(E_ALL) ;
ini_set('display_errors','On') ;
ini_set('display_startup_errors', 1);
date_default_timezone_set("Asia/Taipei"); //設定台北時間
// HTTP
define('SERVER_ADDRESS',	$_SERVER['HTTP_HOST']) ;
define('PROJECT_SITE',		'leka') ;

define('HTTP_SERVER',		'http://'.SERVER_ADDRESS.'/'.PROJECT_SITE.'/');

// HTTPS
define('HTTPS_SERVER',		'http://'.SERVER_ADDRESS.'/'.PROJECT_SITE.'/');

// DIR for Mac
define('DIR_INITIAL',		'/Applications/XAMPP/xamppfiles/htdocs/'.PROJECT_SITE) ;
// DIR for Linux
// define('DIR_INITIAL',		'/Applications/XAMPP/xamppfiles/htdocs/'.PROJECT_SITE) ;
// DIR for Windows
// define('DIR_INITIAL',		'/Applications/XAMPP/xamppfiles/htdocs/'.PROJECT_SITE) ;

// define('DIR_APPLICATION',	DIR_INITIAL.'/catalog/');
// define('DIR_SYSTEM',		DIR_INITIAL.'/system/');
// define('DIR_IMAGE',			DIR_INITIAL.'/image/');
// define('DIR_LANGUAGE',		DIR_INITIAL.'/catalog/language/');
// define('DIR_TEMPLATE',		DIR_INITIAL.'/catalog/view/template/');
// define('DIR_CONFIG',		DIR_INITIAL.'/system/config/');
// define('DIR_CACHE',			DIR_INITIAL.'/system/storage/cache/');
// define('DIR_DOWNLOAD',		DIR_INITIAL.'/system/storage/download/');
// define('DIR_LOGS',			DIR_INITIAL.'/system/storage/logs/');
// define('DIR_MODIFICATION',	DIR_INITIAL.'/system/storage/modification/');
// define('DIR_UPLOAD',		DIR_INITIAL.'/system/storage/upload/');

define('DIR_APPLICATION',	DIR_INITIAL.'/dis/');
define('DIR_SYSTEM',		DIR_INITIAL.'/system/');
define('DIR_IMAGE',			DIR_INITIAL.'/image/');
define('DIR_LANGUAGE',		DIR_INITIAL.'/dis/language/');
define('DIR_TEMPLATE',		DIR_INITIAL.'/dis/view/template/');
define('DIR_CONFIG',		DIR_INITIAL.'/system/config/');
define('DIR_CACHE',			DIR_INITIAL.'/system/storage/cache/');
define('DIR_DOWNLOAD',		DIR_INITIAL.'/system/storage/download/');
define('DIR_LOGS',			DIR_INITIAL.'/system/storage/logs/');
define('DIR_MODIFICATION',	DIR_INITIAL.'/system/storage/modification/');
define('DIR_UPLOAD',		DIR_INITIAL.'/system/storage/upload/');

// DB
define('DB_DRIVER',		'mysqli');
define('DB_HOSTNAME',	'anguskh.com');
define('DB_USERNAME',	'kmfun');
define('DB_PASSWORD',	'kmfun123');
define('DB_DATABASE',	'leka_dev');
// define('DB_DATABASE',	'leka_dev');
define('DB_PORT',		'3306');
define('DB_PREFIX',		'oc_');

// Debug Mod add by Angus 2017.03.20
define('DB_DEBUG_MOD', true) ;
