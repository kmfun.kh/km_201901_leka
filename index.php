<?php
error_reporting(E_ALL) ;
ini_set('display_errors','On') ;
ini_set('display_startup_errors', 1);
date_default_timezone_set("Asia/Taipei"); //設定台北時間

// header("Location: admin/index.php") ;
// exit() ;


// Version
define('VERSION', '2.3.0.3_rc');

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

start('dis');