<?php
class ModelToolReport extends Model {

	/**
	 * [getList 退傭日誌List]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-25
	 */
	public function getList( $data = array()) {
		// dump( $data) ;
		$tbsW[] = "guest_source='{$data['source_name']}'" ;
		if ( !empty($data['filter_syear'])) {
			$tbsW[] = "syear='{$data['filter_syear']}'" ;
		} else {
			$tbsW[] = "syear='".date('Y')."'" ;
		}
		if ( !empty($data['filter_smonth'])) {
			$tbsW[] = "smonth='{$data['filter_smonth']}'" ;
		} else {
			$tbsW[] = "smonth='1'" ;
		}
		if ( !empty( $data['filter_platform'])) {
			$tbsW[]       = "platform='{$data['filter_platform']}'" ;
		}

		$tbsWereStr = " AND " . join(" AND ", $tbsW) ;

		$SQLCmd = "SELECT
				tbs.syear,
				tbs.smonth,
				tbs.sday,
				tbs.cardno,
				tbs.consume,
				tbs.commission,
				tbs.people,
				ROUND( tbs.commission * tbr.main_rate / 100 ) main_rate_calcul,
				ROUND( tbs.commission * tbr.deduct_rate / 100 ) deduct_rate_calcul,
				ROUND( tbs.commission * tbr.kmfun_rate / 100 ) kmfun_rate_calcul,
				ROUND( tbs.commission * tbr.transfer_rate / 100 ) transfer_rate_calcul,
				ROUND( tbs.commission * tbr.agency_rate / 100 ) agency_rate_calcul,
				ROUND( tbs.commission * tbr.other_rate / 100 ) other_rate_calcul
				FROM
					( SELECT guest_source, cardno, syear, smonth, sday, consume, commission, people FROM tb_summary
					WHERE guest_source = '{$data['source_name']}' {$tbsWereStr} ) tbs
					LEFT JOIN tb_resource tbr ON tbs.guest_source = tbr.source_name
					AND tbr.source_name = '{$data['source_name']}' ORDER BY tbs.syear ASC, tbs.smonth ASC, tbs.sday ASC" ;
		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd)->rows ;
	}

	/**
	 * [getRefundGroup 退傭總表]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-01-30
	 */
	public function getRefundLists( $data = array()) {
		// dump( $data) ;
		$tbsW[] = "guest_source='{$data['source_name']}'" ;
		if ( !empty($data['filter_syear'])) {
			$tbsW[] = "syear='{$data['filter_syear']}'" ;
		} else {
			$tbsW[] = "syear='".date('Y')."'" ;
		}
		if ( !empty( $data['filter_platform'])) {
			$tbsW[]       = "platform='{$data['filter_platform']}'" ;
		}

		$tbsWereStr = join(" AND ", $tbsW) ;

		$refundArr = array() ;
		$SQLCmd = "SELECT tbs.guest_source, tbs.syear, tbs.smonth, tbs.sum_consume, tbs.sum_commission, tbs.cnt, tbs.mcnt,
				ROUND(tbs.sum_commission * tbr.main_rate / 100) main_rate_calcul,
				ROUND(tbs.sum_commission * tbr.deduct_rate / 100) deduct_rate_calcul ,
				ROUND(tbs.sum_commission * tbr.kmfun_rate / 100) kmfun_rate_calcul ,
				ROUND(tbs.sum_commission * tbr.transfer_rate / 100) transfer_rate_calcul ,
				ROUND(tbs.sum_commission * tbr.agency_rate / 100) agency_rate_calcul,
				ROUND(tbs.sum_commission * tbr.other_rate / 100) other_rate_calcul
				FROM (
					SELECT guest_source, syear, smonth, COUNT( guest_source) cnt, SUM(consume) sum_consume, SUM(commission) sum_commission, sum(people) mcnt FROM tb_summary
					WHERE $tbsWereStr
					GROUP BY syear, smonth) tbs LEFT JOIN tb_resource tbr
					ON tbs.guest_source=tbr.source_name AND tbr.source_name='{$data['source_name']}'" ;
		// dump( $SQLCmd) ;
		$retArr = $this->db->query( $SQLCmd)->rows ;
		return $retArr ;
	}

	/**
	 * [importSummary 匯入]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-02-18
	 */
	public function importSummary( $data = array()) {
		$cntImports = 0 ; // 成功匯入筆數
		$cntRows    = 0 ; // 本次匯入筆數
		$cntRepeat  = 0 ; // 重覆筆數
		$cntIllegal = 0 ; // 失敗筆數

		// 取得所有退傭單位對照表
		$SQLCmd = "SELECT idx, source_name FROM tb_resource " ;
		$retArr = $this->db->query( $SQLCmd)->rows ;
		$sourArr = array() ;
		foreach ($retArr as $iCnt => $row) {
			$sourArr[$row['idx']] = trim( $row['source_name']) ;
		}

		// 取得 現有日誌資料 比對用
		$SQLCmd = "SELECT idx, syear, smonth, sday, cardno, gs_id FROM tb_summary " ;
		$retArr = $this->db->query( $SQLCmd)->rows ;
		$summaryArr = array() ;
		foreach ($retArr as $iCnt => $row) {
			$tmpStr = trim( $row['syear']).trim( $row['smonth']).trim( $row['sday']).trim( $row['cardno']).trim( $row['gs_id']) ;
			$summaryArr[$row['idx']] = md5( $tmpStr) ;
		}

		// dump( $sourArr) ;
		// dump( $summaryArr) ;
// exit();
		$nonSearchSourceArr = array() ;
		$sourceRepeatArr   = array() ;
		foreach ($data as $rowIndex => $rowCols) {
			$insArr = array() ;

			if ( $rowIndex > 2) {
				$insArr     = $this->makeInsArr( $rowCols) ;
				$source_idx = "" ;
				$isRepeat   = false ;

				// 比對退傭單位id
				foreach ($sourArr as $skey => $source_name) {
					if ( $insArr['guest_source'] == $source_name) {
						$source_idx = $skey ;
					}
				}

				if ( !empty( $source_idx)) {
					$insArr['gs_id'] = $source_idx ;
					// 比對本次匯入是否已重覆
					$tmpStr = md5( trim( $insArr['syear']).trim( $insArr['smonth']).trim( $insArr['sday']).trim( $insArr['cardno']).$insArr['gs_id']) ;
					if ( is_array( $summaryArr)) {
						foreach ($summaryArr as $skey => $md5Str) {
							if ( $tmpStr == $md5Str) $isRepeat = true ;
						}
					} else {
						$isRepeat = true ;
					}
					if ( !$isRepeat) {
						$SQLCmd = $this->makeSqlCmd( $insArr) ;
						// dump( $SQLCmd) ;
						if ( !empty( $SQLCmd)) {
							$this->db->query( $SQLCmd) ;
							$cntImports ++ ;
						}
					} else {
						$cntRepeat ++ ;
						$sourceRepeatArr[] = $insArr ;
					}
				} else {
					// echo "{$insArr['source_name']}<br>" ;
					$nonSearchSourceArr[] = $insArr ;
					$cntIllegal ++ ;
				}
				$cntRows ++ ;
			}
		}
		$retMsg = array(
			"Imports"      => $cntImports,	// 成功匯入筆數
			"Rows"         => $cntRows,		// 本次匯入筆數
			"Repeat"       => $cntRepeat,	// 重覆筆數
			"Illegal"      => $cntIllegal,	// 失敗筆數
			"nonSearch"    => $nonSearchSourceArr,
			"sourceRepeat" => $sourceRepeatArr,
		) ;

		return $retMsg ;
	}

	/**
	 * [makeInsArr description]
	 * @param   array      $rowCol [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2019-02-18
	 */
	public function makeInsArr( $rowCols = array()) {

		$insArr['syear']        = $this->db->escape( trim( $rowCols['A'])) ;
		$insArr['smonth']       = $this->db->escape( trim( $rowCols['B'])) ;
		$insArr['sday']         = $this->db->escape( trim( $rowCols['C'])) ;

		$insArr['cardno']       = $this->db->escape( trim( $rowCols['D'])) ;
		$insArr['makecard']     = $this->db->escape( trim( $rowCols['E'])) ;
		$insArr['guest_source'] = $this->db->escape( trim( $rowCols['F'])) ;
		$insArr['guest_name']   = $this->db->escape( trim( $rowCols['G'])) ;

		$insArr['people']       = $this->db->escape( trim( $rowCols['H'])) ;
		$insArr['consume']      = $this->db->escape( trim( $rowCols['I'])) ;
		$insArr['commission']   = $this->db->escape( trim( $rowCols['J'])) ;
		$insArr['appoint']      = $this->db->escape( trim( $rowCols['K'])) ;

		return $insArr ;
	}

	/**
	 * [makeSqlCmd description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-02-18
	 */
	public function makeSqlCmd( $data = array()) {
		$insStr = "" ;
		foreach ($data as $colKey => $tmpStr) {
			if ( !empty( $tmpStr)) {
				$insStr .= "{$colKey}='{$tmpStr}'," ;
			}
		}
		$insStr = substr( $insStr, 0, strlen( $insStr)-1) ;
		return "INSERT INTO tb_summary SET {$insStr}" ;
	}

	/**
	 * [dashboardTotal description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-10-25
	 */
	public function dashboardTotal( $data = array()) {
		// dump( $data) ;

		$tbsW = array() ;
		if ( !empty($data['source_name'])) {
			$tbsW[] = "guest_source='{$data['source_name']}'" ;
		}
		$tbsWereStr = " AND " . join(" AND ", $tbsW) ;


		$SQLCmd = "SELECT platform, sum(consume) t_consume, sum(commission) t_commission, sum(appoint) t_appoint, sum( people) mcnt FROM tb_summary
				WHERE syear='".date('Y')."' {$tbsWereStr}  GROUP BY platform ORDER BY platform ASC" ;
		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd)->rows ;
	}

}