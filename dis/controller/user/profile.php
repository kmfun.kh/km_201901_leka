<?php
class ControllerUserProfile extends Controller {
	private $error = array();

	public function index() {
		// dump( $this) ;
		$this->load->language('user/user');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/profile');

		$this->getForm();
	}


	protected function getForm() {
		// $this->document->setTitle("日誌匯入 - 樂咖") ;
		$data = array() ;
		$data['title']         = $this->document->getTitle();
		$data['heading_title'] = "個人檔案";
		$data['button_save']   = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['text_title']    = "個人檔案" ;
		$data['text_form']     = "個人檔案" ;
		$data['error_warning'] = "" ;
		$data['success']       = "" ;
		$data['action']        = $this->url->link('user/profile', 'token=' . $this->session->data['token'], true);

		$profile = $this->model_user_profile->getUser( $this->user->getId()) ;
		$data['space'] = "" ;

		// 訊息
		if ( isset( $retMsgArr)) {
			$data['success'] = "本次匯入 {$retMsgArr['Rows']} 筆<br>成功匯入 {$retMsgArr['Imports']} 筆" ;
			if ( $retMsgArr['Repeat'] > 0) {
				$data['error_warning'] .= "本次重覆資料 {$retMsgArr['Repeat']} 筆<br>" ;
			}
			if ( $retMsgArr['Illegal'] > 0) {
				$data['error_warning'] .= "本次資料不全 {$retMsgArr['Illegal']} 筆<br>" ;
			}
		}

		// 基本資料
		$data['source_name']  = $profile['source_name'] ;
		$data['email']        = $profile['email'] ;
		$data['agency_name']  = $profile['agency_name'] ;
		$data['receive']      = $profile['receive'] ;
		$data['account_name'] = $profile['account_name'] ;
		$data['bank_name']    = $profile['bank_name'] ;
		$data['bank_branch']  = $profile['bank_branch'] ;
		$data['bank_code']    = $profile['bank_code'] ;
		$data['bank_account'] = $profile['bank_account'] ;


		$data['breadcrumbs'] = array();
		$url = "" ;
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => "個人檔案",
			'href' => $this->url->link('user/profile', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('user/profile_form', $data));
	}
}