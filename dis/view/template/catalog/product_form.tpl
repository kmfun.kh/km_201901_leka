<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="javascript:void(0);" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary" onclick="checkSubmit();"><i class="fa fa-save"></i></a>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-album"><?php echo $entry_album; ?></label>
                        <div class="col-sm-10">
                            <select name="album_id" class="form-control">
                                <option value="">請選擇</option>
                                <?php foreach($albumArr as $k => $v){?>
                                <option value="<?php echo $k;?>" <?php if($k==$album_id){?>selected<?php }?>><?php echo $v["name"];?></option>
                                <?php }?>
                            </select>
                            <?php if ($error_album) { ?>
                            <div class="text-danger"><?php echo $error_album; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-category"><?php echo $entry_category; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="category" value="" placeholder="<?php echo $entry_category; ?>" id="input-category" class="form-control" />
                            <div id="product-category" class="well well-sm" style="height: 150px; overflow: auto;">
                                <?php foreach ($product_categories as $product_category) { ?>
                                <div id="product-category<?php echo $product_category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_category['name']; ?>
                                <input type="hidden" name="product_category[]" value="<?php echo $product_category['category_id']; ?>" />
                                </div>
                                <?php } ?>
                            </div>
                            <?php if ($error_category) { ?>
                            <div class="text-danger"><?php echo $error_category; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-bib"><span data-toggle="tooltip" title="<?php echo $help_bib; ?>"><?php echo $entry_bib; ?></span></label>
                        <div class="col-sm-10">
                            <input type="text" name="product_bib" value="<?php echo $product_bib; ?>" class="form-control" />
                            <?php if ($error_bib) { ?>
                            <div class="text-danger"><?php echo $error_bib; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
                        <div class="col-sm-10">
                            <a href="javascript:(0);" id="thumb-image" class="img-thumbnail">
                                <img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" width="100" height="100" />
                            </a>
                            <input type="file" name="image" value="" id="input-image" style="display:none;" onchange="imageChange(this);"/>
                            <?php if ($error_image) { ?>
                            <div class="text-danger"><?php echo $error_image; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-cover"><?php echo $entry_cover; ?></label>
                        <div class="col-sm-10">
                            <div class="checkbox">
                                <label><input type="checkbox" name="cover" value="1" id="input-cover" <?php if(isset($albumArr[$album_id]) && $albumArr[$album_id]["cover"]==$product_id){?>checked<?php }?>></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-date"><?php echo $entry_date; ?></label>
                        <div class="col-sm-3">
                            <div class="input-group datetime">
                                <input type="text" name="date_photo" value="<?php echo $date_photo; ?>" placeholder="<?php echo $entry_date; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-date" class="form-control" />
                                <span class="input-group-btn">
                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-size"><?php echo $entry_size; ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="image_length" value="<?php echo $image_length; ?>" placeholder="<?php echo $entry_length; ?>" id="input-size" class="form-control" />
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="image_width" value="<?php echo $image_width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-size" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-dpi"><?php echo $entry_dpi; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="image_dpi" value="<?php echo $image_dpi; ?>" placeholder="dpi" id="input-dpi" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-gps"><?php echo $entry_gps; ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="image_latitude" value="<?php echo $image_latitude; ?>" placeholder="<?php echo $entry_latitude; ?>" id="input-gps" class="form-control" />
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="image_longitude" value="<?php echo $image_longitude; ?>" placeholder="<?php echo $entry_longitude; ?>" id="input-gps" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-photographer"><?php echo $entry_name; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="photographer" value="<?php echo $photographer; ?>" id="input-photographer" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-price"><?php echo $entry_price; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="price" value="<?php echo ceil($price); ?>" id="input-price" class="form-control" />
                            <?php if ($error_price) { ?>
                            <div class="text-danger"><?php echo $error_price; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-points"><?php echo $entry_points; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="points" value="<?php echo $points; ?>" id="input-points" class="form-control" />
                            <?php if ($error_points) { ?>
                            <div class="text-danger"><?php echo $error_points; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                            <select name="status" id="input-status" class="form-control">
                                <option value="0"><?php echo $text_disabled; ?></option>
                                <option value="1" <?php if($status){?>selected<?php }?>><?php echo $text_enabled; ?></option>
                            </select>
                        </div>
                    </div>
                    <?php if(!empty($product_id)){?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-editor"><?php echo $entry_editor; ?></label>
                        <div class="col-sm-10" style="padding-top:9px;"><?php echo $editor_name; ?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-editor"><?php echo $entry_edit_time; ?></label>
                        <div class="col-sm-10" style="padding-top:9px;"><?php echo $editor_time; ?></div>
                    </div>
                    <?php }?>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true,
        maxDate: new Date(),
        sideBySide: true
    });
    // Category
    $('input[name=\'category\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['category_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'category\']').val('');
    
            $('#product-category' + item['value']).remove();
    
            $('#product-category').append('<div id="product-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_category[]" value="' + item['value'] + '" /></div>');
        }
    });
    $('#product-category').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
    });
    $('[name="date_photo"]').change(function(){
        if($(this).val().length > 0){
            if(Date.parse($(this).val()).valueOf()>Date.parse(new Date().toDateString()).valueOf()){
                $(this).val('');
                alert("拍攝日期不可超過今天！");
            }
        }       
    });
    function imageChange(obj){
        var ext = $(obj).val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
            $(obj).val('');
            alert('上傳圖檔格式錯誤！\n\n 請上傳 gif,png,jpg,jpeg 檔案');
            return false;
        }
        if (obj.files && obj.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#thumb-image img').attr('src', e.target.result);
            }
            reader.readAsDataURL(obj.files[0]);
        }
    }
    function checkSubmit(){
        var err = new Array();
        var album = $('[name="album_id"]').val();
        if(album==""){
            err[err.length] = "請選擇相簿";
        }
        var cate = $('[name^="product_category"]').length;
        if(typeof cate == 'undefined'){
            err[err.length] = "請選擇相片分類";
        }
        var bib = $('[name="product_bib"]').val();
        if(bib.length<=0){
            err[err.length] = "請輸入號碼布";
        }
        <?php if(empty($product_id)){?>
        if($('[name="image"]').val()==""){
            err[err.length] = "請上傳相片";
        }
        <?php }?>
        if($('[name="price"]').val()==""){
            $('[name="price"]').val(0);
        }
        if($('[name="points"]').val()==""){
            $('[name="points"]').val(0);
        }

        if(err.length>0){
            alert(err.join('\n'));
            return false;
        }
        else{
            $('#form-product').submit();
        }
    }
    </script>
</div>
<?php echo $footer; ?>
