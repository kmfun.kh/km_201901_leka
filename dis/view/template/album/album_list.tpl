<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-album').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-number"><?php echo $entry_number; ?></label>
                                <input type="text" name="album_number" value="<?php echo $album_number; ?>" placeholder="<?php echo $entry_number; ?>" id="input-number" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-sdate"><?php echo $entry_sdate; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="album_start" value="<?php echo $album_start; ?>" placeholder="<?php echo $entry_sdate; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <select name="album_status" class="form-control" id="input-status">
                                    <option value="">請選擇</option>
                                    <?php 
                                    foreach($statusArr as $k => $v){
                                        if(!$k) continue;
                                    ?>
                                    <option value="<?php echo $k ;?>" <?php if($k==$album_status){?>selected<?php }?>><?php echo $v; ?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                <input type="text" name="album_name" value="<?php echo $album_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-sdate"><?php echo $entry_edate; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="album_end" value="<?php echo $album_end; ?>" placeholder="<?php echo $entry_edate; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group"></div>
                            <button type="button" id="button-clear" class="btn btn-danger pull-right" style="margin-left:5px;"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
                        </div>
                    </div>
                </div>
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-album">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td style="width: 1px;" class="text-center">
                                    <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo $sort_number; ?>" class="<?php echo ($sort=='number'?strtolower($order):''); ?>"><?php echo $column_number; ?></a>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo $sort_name; ?>" class="<?php echo ($sort=='name'?strtolower($order):''); ?>"><?php echo $column_name; ?></a>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo $sort_status; ?>" class="<?php echo ($sort=='status'?strtolower($order):''); ?>"><?php echo $column_status; ?></a>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo $sort_start; ?>" class="<?php echo ($sort=='shoot_start'?strtolower($order):''); ?>"><?php echo $column_start; ?></a>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo $sort_create; ?>" class="<?php echo ($sort=='create_date'?strtolower($order):''); ?>"><?php echo $column_create; ?></a>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo $sort_modify; ?>" class="<?php echo ($sort=='modify_date'?strtolower($order):''); ?>"><?php echo $column_modify; ?></a>
                                </td>
                                <td class="text-center"><?php echo $column_action; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($albums) { ?>
                            <?php foreach ($albums as $album) { ?>
                            <tr>
                                <td class="text-center">
                                    <?php if (in_array($album['album_id'], $selected)) { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $album['album_id']; ?>" checked="checked" />
                                    <?php } else { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $album['album_id']; ?>" />
                                    <?php } ?>
                                </td>
                                <td class="text-center"><?php echo $album['number']; ?></td>
                                <td class="text-center"><?php echo $album['name']; ?></td>
                                <td class="text-center"><?php echo $statusArr[$album['status']]; ?></td>
                                <td class="text-center"><?php echo $album['shoot_start'].(!empty($album['shoot_end'])?'~'.$album['shoot_end']:''); ?></td>
                                <td class="text-center"><?php echo $album['date_added']; ?></td>
                                <td class="text-center"><?php echo $album['date_modified']; ?></td>
                                <td class="text-center">
                                    <a href="<?php echo $album['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.date').datetimepicker({
        pickTime: false
    });
    $('#button-filter').on('click', function() {
        var url = 'index.php?route=album/album&token=<?php echo $token; ?>';
        var album_number = $('input[name=\'album_number\']').val();
        if (album_number) {
            url += '&album_number=' + encodeURIComponent(album_number);
        }
        var album_name = $('input[name=\'album_name\']').val();
        if (album_name) {
            url += '&album_name=' + encodeURIComponent(album_name);
        }
        var album_start = $('input[name=\'album_start\']').val();
        if (album_start) {
            url += '&album_start=' + encodeURIComponent(album_start);
        }
        var album_end = $('input[name=\'album_end\']').val();
        if (album_end) {
            url += '&album_end=' + encodeURIComponent(album_end);
        }
        var album_status = $('[name=\'album_status\']').val();
        if (album_status) {
            url += '&album_status=' + encodeURIComponent(album_status);
        }
        location = url;
    });
    $("#button-clear").click(function(){
        $('input[name="album_number"]').val('');
        $('input[name="album_name"]').val('');
        $('input[name="album_start"]').val('');
        $('input[name="album_end"]').val('');
        $('[name="album_status"]').val('');
    });
});
</script>
<?php echo $footer; ?>