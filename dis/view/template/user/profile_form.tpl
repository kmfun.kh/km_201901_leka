<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-user" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
				<ul class="breadcrumb">
					<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
					<?php } ?>
				</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-user" class="form-horizontal">
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-username">帳號名稱</label>
						<div class="col-sm-10">
							<span class="form-control"><?=$source_name?></span>
							<input type="text" name="username" value="<?php echo $source_name; ?>" placeholder="帳號名稱" id="input-username" class="form-control d-hide" />
							<?php if ($space) { ?>
							<div class="text-danger"><?php echo $space; ?></div>
							<?php } ?>
							</div>
					</div>
<!--
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-username">密碼</label>
						<div class="col-sm-10">

							<input type="text" name="username" value="<?php echo $space; ?>" placeholder="<?php echo $space; ?>" id="input-username" class="form-control d-hide" />
							<?php if ($space) { ?>
							<div class="text-danger"><?php echo $space; ?></div>
							<?php } ?>
							</div>
					</div>
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-username">確認密碼</label>
						<div class="col-sm-10">

							<input type="text" name="username" value="<?php echo $space; ?>" placeholder="<?php echo $space; ?>" id="input-username" class="form-control d-hide" />
							<?php if ($space) { ?>
							<div class="text-danger"><?php echo $space; ?></div>
							<?php } ?>
							</div>
					</div>
-->
					<div class="form-group ">
						<label class="col-sm-2 control-label" for="input-username">email</label>
						<div class="col-sm-10">
							<span class="form-control"><?=$email?></span>
							<input type="text" name="username" value="<?php echo $space; ?>" placeholder="<?php echo $space; ?>" id="input-username" class="form-control d-hide" />
							<?php if ($space) { ?>
							<div class="text-danger"><?php echo $space; ?></div>
							<?php } ?>
							</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label" for="input-username">領取方式</label>
						<div class="col-sm-10">
							<span class="form-control"><?=$receive?></span>
							<input type="text" name="username" value="<?php echo $space; ?>" placeholder="<?php echo $space; ?>" id="input-username" class="form-control d-hide" />
							<?php if ($space) { ?>
							<div class="text-danger"><?php echo $space; ?></div>
							<?php } ?>
							</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label" for="input-username">銀行戶名</label>
						<div class="col-sm-10">
							<span class="form-control"><?=$account_name?></span>
							<input type="text" name="username" value="<?php echo $space; ?>" placeholder="<?php echo $space; ?>" id="input-username" class="form-control d-hide" />
							<?php if ($space) { ?>
							<div class="text-danger"><?php echo $space; ?></div>
							<?php } ?>
							</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label" for="input-username">銀行名稱</label>
						<div class="col-sm-10">
							<span class="form-control"><?=$bank_name?></span>
							<input type="text" name="username" value="<?php echo $space; ?>" placeholder="<?php echo $space; ?>" id="input-username" class="form-control d-hide" />
							<?php if ($space) { ?>
							<div class="text-danger"><?php echo $space; ?></div>
							<?php } ?>
							</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label" for="input-username">分行</label>
						<div class="col-sm-10">
							<span class="form-control"><?=$bank_branch?></span>
							<input type="text" name="username" value="<?php echo $space; ?>" placeholder="<?php echo $space; ?>" id="input-username" class="form-control d-hide" />
							<?php if ($space) { ?>
							<div class="text-danger"><?php echo $space; ?></div>
							<?php } ?>
							</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label" for="input-username">銀行代碼</label>
						<div class="col-sm-10">
							<span class="form-control"><?=$bank_code?></span>
							<input type="text" name="username" value="<?php echo $space; ?>" placeholder="<?php echo $space; ?>" id="input-username" class="form-control d-hide" />
							<?php if ($space) { ?>
							<div class="text-danger"><?php echo $space; ?></div>
							<?php } ?>
							</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label" for="input-username">銀行帳號</label>
						<div class="col-sm-10">
							<span class="form-control"><?=$bank_account?></span>
							<input type="text" name="username" value="<?php echo $space; ?>" placeholder="<?php echo $space; ?>" id="input-username" class="form-control d-hide" />
							<?php if ($space) { ?>
							<div class="text-danger"><?php echo $space; ?></div>
							<?php } ?>
							</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.d-hide').hide() ;
</script>
<?php echo $footer; ?>