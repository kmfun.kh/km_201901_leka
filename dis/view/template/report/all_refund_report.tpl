<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="search-date">查詢平台</label>
								<select class="form-control" name="filter_platform">
									<option value="">請選擇</option>
									<option value="1" <?php if( @$filter_platform == "1"):?>selected<?php endif;?>>昇恆昌</option>
									<option value="2" <?php if( @$filter_platform == "2"):?>selected<?php endif;?>>金坊</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="search-date">查詢年份</label>
								<select class="form-control" name="filter_syear">
									<option value="">請選擇</option>
									<?php for( $i=(int)date('Y'); $i >= 2019; $i--) : ?>
									<option value="<?=$i?>" <?php if( @$filter_syear == $i):?>selected<?php endif;?>><?=$i?>年</option>
									<?php endfor ; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
                            <button type="button" id="button-clear" class="btn btn-danger pull-right" style="margin-left:5px;"><i class="fa fa-eraser"></i>清除</button>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i>查詢</button>
						</div>
                    </div>
                </div>
                <form method="post" action="" enctype="multipart/form-data" id="form-order">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
							<?php foreach ($columnNames as $colKey => $colName) : ?>
							<td class="text-center">
								<?php if ( isset( $colSort[$colKey])) : ?>
									<?php if ( $sort == $colKey) : ?>
									<a href="<?=$colSort[$colKey]?>" class="<?php echo strtolower($order); ?>"><?=$colName?></a>
									<?php else : ?>
									<a href="<?=$colSort[$colKey]?>"><?=$colName?></a>
									<?php endif ; ?>
								<?php else : ?>
								<?=$colName?>
								<?php endif ; ?>
							</td>
							<?php endforeach ; ?>
								<td class="text-center">明細</td>
                        </thead>
                        <tbody>
                            <?php if ($listRows) { ?>
                            <?php foreach ($listRows as $row) { ?>
                            <tr class="viewDetail">
                                <?php foreach ($columnNames as $colKey => $colName) : ?>
                                	<?php if ( $colKey != "syear" && $colKey != "smonth") : ?>
                                <td class="text-center"><?=number_format($row[$colKey])?></td>
                                	<?php else : ?>
                                <td class="text-center"><?=$row[$colKey]?></td>
                                	<?php endif ; ?>
	                            <?php endforeach ; ?>
								<td class="text-center">
									<button type="button" class="btn btn-primary" data-toggle="tooltip" title="檢視" onclick="openModal('<?=$row['makecard']?>')"><i class="fa fa-eye"></i></button>
								</a>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="<?=$td_colspan?>"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
    <script src="dis/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link href="dis/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
	<style type="text/css">
	</style>
    <script type="text/javascript">
	$('.date').datetimepicker({
		pickTime: false
	});


	$('#button-filter').on('click', function() {
		filterSend();
	});
	// 搜尋
	function filterSend(){
		url = '?route=report/personal&token='+getURLVar('token');
		var filter = ['syear', 'platform'];
		for(var i=0;i<filter.length;i++){
			var target = $('[name="filter_'+filter[i]+'"]').val();
			if(target != '') {
				url += '&filter_'+filter[i]+'=' + encodeURIComponent(target);
			}
		}
		location = url;
	}








    // $('.viewDetail').click( function ()) {
    //     console.log('test');
    // });
    $('.viewDetail').mouseover( function () {
        $(this).css( "cursor", "pointer") ;
    });

    $('.viewDetail').click(function (){
        var url = 'index.php?route=report/personal/monthDetail&token='+getURLVar('token');
        var filter = ['syear', 'platform'];
		for(var i=0;i<filter.length;i++){
			var target = $('[name="filter_'+filter[i]+'"]').val();
			if(target != '') {
				url += '&filter_'+filter[i]+'=' + encodeURIComponent(target);
			}
		}
		// var syear  = $(this).find("td").eq(0).html() ;
        var smonth = $(this).find("td").eq(1).html() ;
        // console.log( syear);
        console.log( smonth);
        location = url + "&filter_smonth="+smonth;
    });

	// $('#button-filter').on('click', function() {
	// 	console.log('button-filter');
	// 	url = 'index.php?route=report/personal&token=<?php echo $token; ?>';
	// 	var search_year = $('[name="search_year"]').val();
	// 	if (search_year) {
	// 		url += '&search_year=' + encodeURIComponent(search_year);
	// 	}

	// 	location = url;
	// });
	// $("#button-clear").click(function(){
	// 	console.log('button-clear');
	// 	$('[name="search_year"]').val('');
	// });
    </script>
</div>
<?php echo $footer; ?>