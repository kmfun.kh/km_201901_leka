<?php
error_reporting(E_ALL) ;
ini_set("display_errors","On") ;
date_default_timezone_set("Asia/Taipei") ;
mb_internal_encoding("UTF-8");
ini_set("memory_limit","2048M");
session_start();

require_once('database_inc.php');
// $db = new database(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT) ;



function save_log( $msg, $fileName) {
	$folder = "log" ;
	if ( !is_dir( $folder)) {
		mkdir( $folder) ;
	}
	$fileDate = date( "Y_md" ) ;
	$logFileName = "{$folder}/{$fileName}_{$fileDate}.log" ;
	$fp = fopen($logFileName , 'a' ) ;
	$nowTime = date("Y/m/d H:i:s");

	if (is_array($msg)) {
		fwrite($fp,"{$nowTime} | ".print_r($msg, true)."\r\n") ;
		// fwrite($fp,print_r($msg, true)."\r\n") ;
	} else {
		fwrite($fp,"{$nowTime} | {$msg}\r\n") ;
		// fwrite($fp,"{$msg}\r\n") ;
	}
	 fclose($fp);
}